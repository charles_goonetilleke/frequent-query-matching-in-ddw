package MaintenanceExperiments;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import querytree.MaterializedViewQueryTree;
import querytree.enums.StatusEnum;
import querytree.utilities.DBConnection;

public class Analyser extends Thread {

    public void run() {
        try {
            System.out.println("Secondary :\n" + MaterializedViewQueryTreeCollection.secondaryCollection_id_as_key.toString());
            LogHitCounters();

            if (MaterializedViewQueryTreeCollection.sessionId == Constants.SESSION_THRESHOLD) {

                ArrayList<MaterializedViewQueryTree> promote = new ArrayList<MaterializedViewQueryTree>();
                ArrayList<MaterializedViewQueryTree> demote = new ArrayList<MaterializedViewQueryTree>();
                ArrayList<MaterializedViewQueryTree> delete = new ArrayList<MaterializedViewQueryTree>();

                MaterializedViewQueryTreeCollection.primaryCollection_id_as_key.forEach((Integer k, MaterializedViewQueryTree v) -> {
                    try (ResultSet rs = DBConnection.ExecuteQuery("SELECT * FROM " + Constants.HIT_COUNTER_TABLE
                            + " WHERE ANALYSERRUNID = "
                            + MaterializedViewQueryTreeCollection.analyserRunId
                            + " AND QUERYTREEID=" + k)) {
                        int sum_of_hits = 0;

                        while (rs.next()) {
                            sum_of_hits += rs.getInt(5);
                        }

                        if (sum_of_hits == 0) {
                            demote.add(v);
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
                        return;
                    }
                });

                MaterializedViewQueryTreeCollection.secondaryCollection_id_as_key.forEach((Integer k, MaterializedViewQueryTree v) -> {
                    try (ResultSet rs = DBConnection.ExecuteQuery("SELECT * FROM " + Constants.HIT_COUNTER_TABLE
                            + " WHERE ANALYSERRUNID = "
                            + MaterializedViewQueryTreeCollection.analyserRunId
                            + " AND QUERYTREEID=" + k)) {
                        int count = 0;

                        while (rs.next()) {
                            if (rs.getInt(5) >= 1) {
                                ++count;
                            }
                        }

                        if (count == Constants.SESSION_THRESHOLD) {
                            promote.add(v);

                        } else if (v.status == StatusEnum.SOFT_DELETED && count == 0) {
                            delete.add(v);
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
                        return;
                    }
                });

                for (MaterializedViewQueryTree mv : promote) {
                    try {
                        MaterializedViewQueryTreeCollection.ReActivateMaterializedView(mv);
                        System.out.println(mv.toString() + " promoted to primary b+ tree");
                    } catch (Exception ex) {
                        Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                for (MaterializedViewQueryTree mv : demote) {
                    try {
                        MaterializedViewQueryTreeCollection.SoftDeleteMaterializedView(mv);
                        System.out.println(mv.toString() + " was soft deleted");
                    } catch (Exception ex) {
                        Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                for (MaterializedViewQueryTree mv : delete) {
                    MaterializedViewQueryTreeCollection.DeleteMaterializedViewQueryTreeFromSecondary(mv);
                    System.out.println(mv.toString() + " was deleted permanently");
                }

                ++MaterializedViewQueryTreeCollection.analyserRunId;
                MaterializedViewQueryTreeCollection.sessionId = 1;

                try {
                    DBConnection.Disconnect();
                } catch (SQLException ex) {
                    Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                // increment session id for new session
                MaterializedViewQueryTreeCollection.sessionId++;
            }
        } catch (Exception ex) {
            Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.out.println("Primary :\n" + MaterializedViewQueryTreeCollection.primaryCollection.toString());
            System.out.println("Secondary :\n" + MaterializedViewQueryTreeCollection.secondaryCollection.toString());
        }
    }

    public static void LogHitCounters() {
        MaterializedViewQueryTreeCollection.primaryCollection_id_as_key.forEach((Integer k, MaterializedViewQueryTree v) -> {
            try {
                InsertToLog(v);
                v.ResetHits();
            } catch (SQLException ex) {
                Logger.getLogger(MaterializedViewQueryTreeCollection.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        MaterializedViewQueryTreeCollection.secondaryCollection_id_as_key.forEach((Integer k, MaterializedViewQueryTree v) -> {
            try {
                InsertToLog(v);
                v.ResetHits();
            } catch (SQLException ex) {
                Logger.getLogger(MaterializedViewQueryTreeCollection.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        MaterializedViewQueryTreeCollection.count = 0;

        try {
            DBConnection.Disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Session reset complete");
    }

    private static void InsertToLog(MaterializedViewQueryTree mvqt) throws SQLException {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ").append(Constants.HIT_COUNTER_TABLE)
                .append(" VALUES (")
                .append(MaterializedViewQueryTreeCollection.analyserRunId)
                .append(",")
                .append(MaterializedViewQueryTreeCollection.sessionId)
                .append(",")
                .append(mvqt.id)
                .append(",")
                .append("'").append(mvqt.status).append("'")
                .append(",")
                .append(mvqt.currentSessionHits)
                .append(",")
                .append("'").append(mvqt.name).append("'")
                .append(",")
                .append(MaterializedViewQueryTreeCollection.count)
                .append(")");
        DBConnection.ExecuteUpdate(sql.toString());
    }
}
