package MaintenanceExperiments;

import querytree.InputQueryTree;
import querytree.QueryMatching;
import querytree.MaterializedViewQueryTree;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.HashMap;
import java.util.Set;
import querytree.enums.StatusEnum;

public final class MaterializedViewQueryTreeCollection {

    public static ConcurrentHashMap<String, ArrayList<MaterializedViewQueryTree>> primaryCollection;
    public static ConcurrentHashMap<Integer, MaterializedViewQueryTree> primaryCollection_id_as_key;
    public static ConcurrentHashMap<String, ArrayList<MaterializedViewQueryTree>> secondaryCollection;
    public static ConcurrentHashMap<Integer, MaterializedViewQueryTree> secondaryCollection_id_as_key;
    public static int sessionId;
    public static int analyserRunId;
    public static int count;
    public static long start;
    public static long end;

    public static void Init() throws SQLException {
        // HashMap with table names as keys
        // The value of each key is a list of query trees that have that tree
        primaryCollection = new ConcurrentHashMap<String, ArrayList<MaterializedViewQueryTree>>();

        primaryCollection_id_as_key = new ConcurrentHashMap<Integer, MaterializedViewQueryTree>();

        // Store new query trees and soft-deleted query trees
        secondaryCollection = new ConcurrentHashMap<String, ArrayList<MaterializedViewQueryTree>>();

        secondaryCollection_id_as_key = new ConcurrentHashMap<Integer, MaterializedViewQueryTree>();

        sessionId = 1;

        analyserRunId = 1;
        
        count = 0;
    }

    public static ArrayList<MaterializedViewQueryTree> DomainMatching(InputQueryTree input, ConcurrentHashMap<String, ArrayList<MaterializedViewQueryTree>> collection) throws Exception {
        ArrayList<String> tableNames = input.root.GetTableNames();

        HashMap<String, ArrayList<MaterializedViewQueryTree>> treesOfTableNames = new HashMap<String, ArrayList<MaterializedViewQueryTree>>();
        for (String tableName : tableNames) {
            ArrayList<MaterializedViewQueryTree> value = collection.get(tableName);
            if (value != null) {
                treesOfTableNames.put(tableName, value);
            } else {
                treesOfTableNames.put(tableName, new ArrayList<MaterializedViewQueryTree>());
            }
        }

        HashMap<MaterializedViewQueryTree, Integer> mvqtsWithCounts = new HashMap<MaterializedViewQueryTree, Integer>();
        for (String tableName : tableNames) {
            ArrayList<MaterializedViewQueryTree> mvqtsOfTableName = treesOfTableNames.get(tableName);
            for (MaterializedViewQueryTree mvqt : mvqtsOfTableName) {
                Integer i = mvqtsWithCounts.get(mvqt);
                if (i == null) {
                    mvqtsWithCounts.put(mvqt, 1);
                } else {
                    mvqtsWithCounts.replace(mvqt, ++i);
                }
            }
        }

        ArrayList<MaterializedViewQueryTree> mvqtsWithSameTables = new ArrayList<MaterializedViewQueryTree>();
        Set<MaterializedViewQueryTree> mvqts = mvqtsWithCounts.keySet();
        for (MaterializedViewQueryTree mvqt : mvqts) {
            Integer i = mvqtsWithCounts.get(mvqt);
            if (i == tableNames.size()) {
                mvqtsWithSameTables.add(mvqt);
            }
        }
        return mvqtsWithSameTables;
    }

    public static ArrayList<MaterializedViewQueryTree> FindMatch(InputQueryTree input) throws Exception {
        ++count;
        ArrayList<MaterializedViewQueryTree> mvqtsWithSameTablesInPrimaryCollection = DomainMatching(input, primaryCollection);

        ArrayList<MaterializedViewQueryTree> mvqtsThatMatchInPrimaryCollection = new ArrayList<MaterializedViewQueryTree>();
        for (MaterializedViewQueryTree mvqt : mvqtsWithSameTablesInPrimaryCollection) {
            double score = new QueryMatching(mvqt, input).Calculate();

            if (score == 4) {
                mvqt.currentSessionHits++;
                mvqtsThatMatchInPrimaryCollection.add(mvqt);
            }
        }

        // look in secondary collection
        ArrayList<MaterializedViewQueryTree> mvqtsThatMatchInSecondaryCollection = new ArrayList<MaterializedViewQueryTree>();
        if (mvqtsThatMatchInPrimaryCollection.isEmpty()) {
            ArrayList<MaterializedViewQueryTree> mvqtsWithSameTablesInSecondaryCollection = DomainMatching(input, secondaryCollection);

            for (MaterializedViewQueryTree mvqt : mvqtsWithSameTablesInSecondaryCollection) {
                double score = new QueryMatching(mvqt, input).Calculate();

                if (score == 4) {
                    mvqt.currentSessionHits++;
                    mvqtsThatMatchInSecondaryCollection.add(mvqt);
                }
            }
        } else {
            System.out.println("\nMatching materialized views found in primary:");
            for (MaterializedViewQueryTree materializedViewQueryTree : mvqtsThatMatchInPrimaryCollection) {
                System.out.println(materializedViewQueryTree.name);
            }

            return mvqtsThatMatchInPrimaryCollection;
        }

        if (mvqtsThatMatchInSecondaryCollection.isEmpty()) { // not found in both primary and secondary collections
            MaterializedViewQueryTree mv = new MaterializedViewQueryTree(input.sql, input.root);
            mv.status = StatusEnum.INACTIVE;

            ArrayList<String> tableNames = mv.root.GetTableNames();
            for (String tableName : tableNames) {
                ArrayList<MaterializedViewQueryTree> mvqtsOfTableName = secondaryCollection.get(tableName);
                if (mvqtsOfTableName != null) {
                    if (!mvqtsOfTableName.contains(mv)) {
                        mvqtsOfTableName.add(mv);
                    }
                } else {
                    mvqtsOfTableName = new ArrayList<>();
                    mvqtsOfTableName.add(mv);
                    secondaryCollection.put(tableName, mvqtsOfTableName);
                }
            }

            ++mv.currentSessionHits;
            secondaryCollection_id_as_key.putIfAbsent(mv.id, mv);
            System.out.println("New MV query tree added to secondary collection");
        } else {
            System.out.println("\nMatching materialized views found in secondary:");
            for (MaterializedViewQueryTree materializedViewQueryTree : mvqtsThatMatchInSecondaryCollection) {
                System.out.println(materializedViewQueryTree.name);
            }
        }

        return mvqtsThatMatchInSecondaryCollection;
    }

    public static void AddMaterializedViewToPrimary(MaterializedViewQueryTree tree) throws Exception {
        AddMaterializedViewToPrimaryCollection(tree);
        primaryCollection_id_as_key.putIfAbsent(tree.id, tree);
        System.out.println("MV query tree added to primary b+ tree");
    }

    private static void AddMaterializedViewToPrimaryCollection(MaterializedViewQueryTree tree) throws Exception {
        tree.status = StatusEnum.ACTIVE;
        ArrayList<String> tableNames = tree.root.GetTableNames();
        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> value = primaryCollection.get(table);
            if (value != null) {
                if (!value.contains(tree)) {
                    value.add(tree);
                }
            } else {
                value = new ArrayList<>();
                value.add(tree);
                primaryCollection.put(table, value);
            }
        }
    }

    public static void SoftDeleteMaterializedView(MaterializedViewQueryTree tree) throws Exception {
        tree.status = StatusEnum.SOFT_DELETED;

        ArrayList<String> tableNames = tree.root.GetTableNames();

        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> value = primaryCollection.get(table);
            value.remove(tree);
        }

        primaryCollection_id_as_key.remove(tree.id);

        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> value = secondaryCollection.get(table);
            if (value != null) {
                if (!value.contains(tree)) {
                    value.add(tree);
                }
            } else {
                value = new ArrayList<>();
                value.add(tree);
                secondaryCollection.put(table, value);
            }
        }

        secondaryCollection_id_as_key.putIfAbsent(tree.id, tree);
        System.out.println("MV query tree soft deleted");
    }

    public static void DeleteMaterializedViewQueryTreeFromSecondary(MaterializedViewQueryTree tree) throws Exception {
        ArrayList<String> tableNames = tree.root.GetTableNames();

        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> value = secondaryCollection.get(table);
            value.remove(tree);
        }

        secondaryCollection_id_as_key.remove(tree.id);
    }

    public static void ReActivateMaterializedView(MaterializedViewQueryTree tree) throws Exception {
        AddMaterializedViewToPrimary(tree);
        DeleteMaterializedViewQueryTreeFromSecondary(tree);
    }
}
