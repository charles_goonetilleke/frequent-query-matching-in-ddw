package MaintenanceExperiments;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import querytree.InputQueryTree;
import querytree.MaterializedViewQueryTree;
import querytree.enums.AggregateFunctionEnum;
import querytree.enums.RelationalOperatorEnum;
import querytree.enums.TableTypeEnum;
import querytree.structure.CartesianProductOperatorNode;
import querytree.structure.GroupByOperatorNode;
import querytree.structure.ProjectionOperatorNode;
import querytree.structure.SelectionOperatorNode;
import querytree.structure.TableNode;
import querytree.structure.conditions.AggregateFunction;
import querytree.structure.conditions.GroupByCondition;
import querytree.structure.conditions.ProjectionCondition;
import querytree.structure.conditions.SelectionCondition;
import querytree.structure.conditions.Statement;

public class Tests {

    private static final TableNode ADMISSIONS_NODE = new TableNode("Admissions", TableTypeEnum.FACT, "A", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category");
    private static final TableNode DIAGNOSIS_NODE = new TableNode("Diagnosis", TableTypeEnum.DIMENSION, "D", "admissions_pk", "position", "diag", "pref");
    private static final TableNode DIAGNOSIS_DIM_NODE = new TableNode("DIAGNOSIS_DIM", TableTypeEnum.DIMENSION, "DM", "diagnosis_num", "diagnosis_desc", "code_range_start", "code_range_end");
    private static final TableNode ADMSOURCE_DIM_NODE = new TableNode("ADMSOURCE_DIM", TableTypeEnum.DIMENSION, "ADS", "vaed_value", "cd_desc");
    private static final TableNode ADMTYPE_DIM_NODE = new TableNode("ADMTYPE_DIM", TableTypeEnum.DIMENSION, "ATP", "vaed_value", "cd_desc");
    private static final TableNode CARE_TYPE_DIM_NODE = new TableNode("CARE_TYPE_DIM", TableTypeEnum.DIMENSION, "C", "vaed_value", "cd_desc");
    private static final TableNode SEPARATION_MODE_DIM_NODE = new TableNode("SEPARATION_MODE_DIM", TableTypeEnum.DIMENSION, "S", "vaed_value", "cd_desc");
    private static final TableNode ADMTIME_DIM_NODE = new TableNode("ADMTIME_DIM", TableTypeEnum.DIMENSION, "ADT", "admtimeid", "admyear", "admmonth");
    private static final TableNode AGE_DIM_NODE = new TableNode("AGE_DIM", TableTypeEnum.DIMENSION, "AG", "age_category", "age_desc", "start_age", "end_age");
    private static final TableNode GENDER_DIM_NODE = new TableNode("GENDER_DIM", TableTypeEnum.DIMENSION, "G", "gender_number", "gender_desc");

    public static void main(String[] args) {
        test1();
    }

    public static void test1() {
        try {
            MaterializedViewQueryTreeCollection.Init();
            CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
            CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
            CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
            CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
            CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
            CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
            CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
            CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
            CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
            SelectionCondition selectionCondition = new SelectionCondition(
                    new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO));
            SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
            GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY");
            groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
            GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
            ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY");
            projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
            ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
            MaterializedViewQueryTreeCollection.AddMaterializedViewToPrimary(new MaterializedViewQueryTree(projectionNode));

            cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
            cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
            cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
            cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
            cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
            cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
            cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
            cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
            cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
            selectionCondition = new SelectionCondition(
                    new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                    new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO));
            selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
            groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY");
            groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
            groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
            projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY");
            projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
            projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

            ArrayList<MaterializedViewQueryTree> result = MaterializedViewQueryTreeCollection.DomainMatching(new InputQueryTree(projectionNode), MaterializedViewQueryTreeCollection.primaryCollection);
            System.out.println(result.size());

        } catch (Exception ex) {
            Logger.getLogger(Tests.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
