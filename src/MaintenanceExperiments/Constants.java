package MaintenanceExperiments;

public final class Constants {
    public static final String HIT_COUNTER_TABLE = "HIT_COUNTER_TABLE8";
    
    public static int SESSION_LENGTH = 90 * 1000; // in milliseconds
    
    public static int SESSION_THRESHOLD = 2;
    
    public static int RUNTIME = 120; // in minutes
    
    public static int QUERY_FREQUENCY = 30 * 1000; // in milliseconds
}
