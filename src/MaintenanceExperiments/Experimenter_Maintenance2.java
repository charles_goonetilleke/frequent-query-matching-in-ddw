package MaintenanceExperiments;

import querytree.structure.conditions.AggregateFunction;
import querytree.structure.conditions.Statement;
import querytree.structure.conditions.ProjectionCondition;
import querytree.structure.conditions.SelectionCondition;
import querytree.structure.conditions.GroupByCondition;
import querytree.structure.SelectionOperatorNode;
import querytree.structure.ProjectionOperatorNode;
import querytree.structure.GroupByOperatorNode;
import querytree.structure.TableNode;
import querytree.structure.CartesianProductOperatorNode;
import querytree.enums.AggregateFunctionEnum;
import querytree.enums.TableTypeEnum;
import querytree.enums.RelationalOperatorEnum;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import querytree.InputQueryTree;
import querytree.MaterializedViewQueryTree;
import querytree.utilities.DBConnection;

public class Experimenter_Maintenance2 extends Application {

    private Timer analyserTimer;
    private Timer programTimer;

    private static final TableNode ADMISSIONS_NODE = new TableNode("Admissions", TableTypeEnum.FACT, "A", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category");
    private static final TableNode DIAGNOSIS_NODE = new TableNode("Diagnosis", TableTypeEnum.DIMENSION, "D", "admissions_pk", "position", "diag", "pref");
    private static final TableNode DIAGNOSIS_DIM_NODE = new TableNode("DIAGNOSIS_DIM", TableTypeEnum.DIMENSION, "DM", "diagnosis_num", "diagnosis_desc", "code_range_start", "code_range_end");
    private static final TableNode ADMSOURCE_DIM_NODE = new TableNode("ADMSOURCE_DIM", TableTypeEnum.DIMENSION, "ADS", "vaed_value", "cd_desc");
    private static final TableNode ADMTYPE_DIM_NODE = new TableNode("ADMTYPE_DIM", TableTypeEnum.DIMENSION, "ATP", "vaed_value", "cd_desc");
    private static final TableNode CARE_TYPE_DIM_NODE = new TableNode("CARE_TYPE_DIM", TableTypeEnum.DIMENSION, "C", "vaed_value", "cd_desc");
    private static final TableNode SEPARATION_MODE_DIM_NODE = new TableNode("SEPARATION_MODE_DIM", TableTypeEnum.DIMENSION, "S", "vaed_value", "cd_desc");
    private static final TableNode ADMTIME_DIM_NODE = new TableNode("ADMTIME_DIM", TableTypeEnum.DIMENSION, "ADT", "admtimeid", "admyear", "admmonth");
    private static final TableNode AGE_DIM_NODE = new TableNode("AGE_DIM", TableTypeEnum.DIMENSION, "AG", "age_category", "age_desc", "start_age", "end_age");
    private static final TableNode GENDER_DIM_NODE = new TableNode("GENDER_DIM", TableTypeEnum.DIMENSION, "G", "gender_number", "gender_desc");

    private static final ArrayList<InputQueryTree> iqts = new ArrayList<InputQueryTree>();

    @Override
    public void start(Stage stage) throws Exception {
        InitializeSystem();
        addContents(stage);
        stage.setResizable(true);
        stage.setTitle(getClass().getName());
        stage.show();
    }

    @Override
    public void stop() {
        if (this.analyserTimer != null) {
            this.analyserTimer.cancel();
            this.analyserTimer.purge();
            this.analyserTimer = null;
        }

        if (this.programTimer != null) {
            this.programTimer.cancel();
            this.programTimer.purge();
            this.programTimer = null;
        }
    }

    public void InitializeSystem() throws SQLException, Exception {
        MaterializedViewQueryTreeCollection.Init();
        CreateOrTruncateTable();
        iqts.add(Query1());
        iqts.add(Query2());
        iqts.add(Query3());
        iqts.add(Query4());
        iqts.add(Query5());
        iqts.add(Query6());
        iqts.add(Query7());
        iqts.add(ManyJoins());
        iqts.add(ManyGroupBys());
        iqts.add(ManySelections());
        iqts.add(ManyJoinsSelectionsGroupBys());
        iqts.add(SpecialFunctionInSelection());
        iqts.add(To_CharInGroupBy());

        // debugging purposes
        System.out.println("Primary :\n" + MaterializedViewQueryTreeCollection.primaryCollection.toString());
        System.out.println("Secondary :\n" + MaterializedViewQueryTreeCollection.secondaryCollection.toString());
    }

    private InputQueryTree Query1() throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT1", projectionNode);
    }

    private InputQueryTree Query2() throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, ADMTIME_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin1);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT2", projectionNode);
    }

    private InputQueryTree Query3() throws Exception {
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYY')", "'2007'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'MM')", "'12'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'DD')", "'10'", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, ADMISSIONS_NODE);
        GroupByCondition groupByCondition = new GroupByCondition("A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT3", projectionNode);
    }

    private InputQueryTree Query4() throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT4", projectionNode);
    }

    private InputQueryTree Query5() throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin2);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT5", projectionNode);
    }

    private InputQueryTree Query6() throws Exception {
        GroupByCondition groupByCondition = new GroupByCondition("TO_CHAR(ADMDATE, 'YYYY')");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, ADMISSIONS_NODE);
        ProjectionCondition projectionCondition = new ProjectionCondition("TO_CHAR(ADMDATE, 'YYYY')");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT6", projectionNode);
    }

    private InputQueryTree Query7() throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, CARE_TYPE_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin1);
        GroupByCondition groupByCondition = new GroupByCondition("C.VAED_VALUE");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("C.VAED_VALUE");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT7", projectionNode);
    }

    public static InputQueryTree ManyJoins() throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT8", projectionNode);
    }

    public static InputQueryTree ManyGroupBys() throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, ADMTIME_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin1);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT9", projectionNode);
    }

    public static InputQueryTree ManySelections() throws Exception {
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYY')", "'2007'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'MM')", "'12'", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, ADMISSIONS_NODE);
        GroupByCondition groupByCondition = new GroupByCondition("A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT10", projectionNode);
    }

    public static InputQueryTree ManyJoinsSelectionsGroupBys() throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT11", projectionNode);
    }

    public static InputQueryTree SpecialFunctionInSelection() throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin2);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT12", projectionNode);
    }

    public static InputQueryTree To_CharInGroupBy() throws Exception {
        GroupByCondition groupByCondition = new GroupByCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, ADMISSIONS_NODE);
        ProjectionCondition projectionCondition = new ProjectionCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return new InputQueryTree("IQT13", projectionNode);
    }

    public void addContents(Stage stage) throws Exception {
        Button executeAnalyser = new Button("Execute Analyser");
        executeAnalyser.setOnAction((e) -> {
            try {
                MaterializedViewQueryTreeCollection.start = System.currentTimeMillis();
                MaterializedViewQueryTreeCollection.end = MaterializedViewQueryTreeCollection.start + (Constants.RUNTIME * 60000);

                //Analyser code
                this.analyserTimer = new Timer();
                this.analyserTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (System.currentTimeMillis() < MaterializedViewQueryTreeCollection.end) {
                            new Analyser().start();
                        }
                    }
                }, Constants.SESSION_LENGTH, Constants.SESSION_LENGTH);

                this.programTimer = new Timer();
                this.programTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            if (System.currentTimeMillis() < MaterializedViewQueryTreeCollection.end) {
                                MaterializedViewQueryTreeCollection.FindMatch(iqts.get(new Random().nextInt(iqts.size())));
                            } else {
                                System.out.println("Exit program");
                                Platform.exit();
                            }
                        } catch (Exception ex) {
                            Logger.getLogger(Experimenter_Maintenance.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }, Constants.QUERY_FREQUENCY, Constants.QUERY_FREQUENCY);
            } catch (Exception ex) {
                Logger.getLogger(Experimenter_Maintenance2.class.getName()).log(Level.SEVERE, null, ex);
            }
            executeAnalyser.setDisable(true);
        });

        HBox executeHBox = new HBox(executeAnalyser);
        executeHBox.setAlignment(Pos.CENTER);
        executeHBox.setSpacing(10);

        HBox top = new HBox();
        HBox bottom = new HBox();
        top.minHeight(10);
        bottom.minHeight(10);

        VBox vbox = new VBox(top, executeHBox, bottom);

        vbox.setSpacing(10);
        vbox.setStyle("-fx-background-color: wheat; -fx-font-size: 20");
        vbox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vbox, 200, 100);
        stage.setScene(scene);
    }

    public static void CreateOrTruncateTable() throws SQLException {
        try {
            DBConnection.ExecuteQuery("TRUNCATE TABLE " + Constants.HIT_COUNTER_TABLE);
        } catch (Exception e) {
            StringBuilder sql = new StringBuilder();
            sql.append("CREATE TABLE ");
            sql.append(Constants.HIT_COUNTER_TABLE);
            sql.append("(ANALYSERRUNID NUMBER NOT NULL");
            sql.append(",SESSIONID NUMBER NOT NULL");
            sql.append(",QUERYTREEID NUMBER NOT NULL");
            sql.append(",STATUS VARCHAR2(25) NOT NULL");
            sql.append(",HITS NUMBER NOT NULL");
            sql.append(",QUERYTREENAME VARCHAR2(25) NOT NULL");
            sql.append(",COUNT NUMBER NOT NULL");
            sql.append(",CONSTRAINT ");
            sql.append(Constants.HIT_COUNTER_TABLE);
            sql.append("_PK PRIMARY KEY");
            sql.append("(ANALYSERRUNID,QUERYTREEID,SESSIONID))");
            DBConnection.ExecuteQuery(sql.toString());
        }
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
