package MaintenanceExperiments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class TableStats {

    public String table_name;
    public int num_rows;
    public ArrayList<String> columns;

    public TableStats(String table_name, int num_rows, String... columns) {
        this.table_name = table_name;
        this.num_rows = num_rows;
        this.columns = new ArrayList<String>(Arrays.asList(columns));
    }

    @Override
    public String toString() {
        return table_name + " " + num_rows + " " + columns.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || o.getClass() != TableStats.class) {
            return false;
        } else {
            TableStats t = (TableStats) o;
            return this.num_rows == t.num_rows && this.columns.containsAll(t.columns);
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.table_name);
        hash = 71 * hash + this.num_rows;
        hash = 71 * hash + Objects.hashCode(this.columns);
        return hash;
    }
}
