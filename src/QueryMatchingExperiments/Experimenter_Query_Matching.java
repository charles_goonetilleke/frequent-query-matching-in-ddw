package QueryMatchingExperiments;

import querytree.structure.conditions.AggregateFunction;
import querytree.structure.conditions.Statement;
import querytree.structure.conditions.ProjectionCondition;
import querytree.structure.conditions.SelectionCondition;
import querytree.structure.conditions.GroupByCondition;
import querytree.structure.SelectionOperatorNode;
import querytree.structure.ProjectionOperatorNode;
import querytree.structure.GroupByOperatorNode;
import querytree.structure.TableNode;
import querytree.structure.CartesianProductOperatorNode;
import querytree.enums.AggregateFunctionEnum;
import querytree.enums.TableTypeEnum;
import querytree.enums.RelationalOperatorEnum;
import querytree.InputQueryTree;
import querytree.MaterializedViewQueryTree;
import java.io.File;
import java.io.PrintWriter;
import querytree.utilities.DBConnection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Experimenter_Query_Matching extends Application {

    private static String Admissions = "";
    private static String Diagnosis = "";

    private static final TableNode DIAGNOSIS_DIM_NODE = new TableNode("DIAGNOSIS_DIM", TableTypeEnum.DIMENSION, "DM", "diagnosis_num", "diagnosis_desc", "code_range_start", "code_range_end");
    private static final TableNode ADMSOURCE_DIM_NODE = new TableNode("ADMSOURCE_DIM", TableTypeEnum.DIMENSION, "ADS", "vaed_value", "cd_desc");
    private static final TableNode ADMTYPE_DIM_NODE = new TableNode("ADMTYPE_DIM", TableTypeEnum.DIMENSION, "ATP", "vaed_value", "cd_desc");
    private static final TableNode CARE_TYPE_DIM_NODE = new TableNode("CARE_TYPE_DIM", TableTypeEnum.DIMENSION, "C", "vaed_value", "cd_desc");
    private static final TableNode SEPARATION_MODE_DIM_NODE = new TableNode("SEPARATION_MODE_DIM", TableTypeEnum.DIMENSION, "S", "vaed_value", "cd_desc");
    private static final TableNode ADMTIME_DIM_NODE = new TableNode("ADMTIME_DIM", TableTypeEnum.DIMENSION, "ADT", "admtimeid", "admyear", "admmonth");
    private static final TableNode AGE_DIM_NODE = new TableNode("AGE_DIM", TableTypeEnum.DIMENSION, "AG", "age_category", "age_desc", "start_age", "end_age");
    private static final TableNode GENDER_DIM_NODE = new TableNode("GENDER_DIM", TableTypeEnum.DIMENSION, "G", "gender_number", "gender_desc");

    @Override
    public void start(Stage stage) throws Exception {
        addContents(stage);
        stage.setResizable(true);
        stage.setTitle(getClass().getName());
        stage.show();
    }

    public static ProjectionOperatorNode ManyJoins(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode ManyGroupBys(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, ADMTIME_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin1);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode ManySelections(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYY')", "'2007'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'mm')", "'12'", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, ADMISSIONS_NODE);
        GroupByCondition groupByCondition = new GroupByCondition("A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode ManyJoinsSelectionsGroupBys(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode SpecialFunctionInSelection(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin2);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode To_CharInGroupBy(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        SelectionCondition selectionCondition = new SelectionCondition();
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, ADMISSIONS_NODE);
        GroupByCondition groupByCondition = new GroupByCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    private void InitializeMVCollection(String partition, TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        MaterializedViewQueryTreeCollection.Init();

        try {
            DBConnection.ExecuteUpdate("DROP MATERIALIZED VIEW MV1");
        } catch (Exception e) {
        }

        try {
            DBConnection.ExecuteUpdate("DROP MATERIALIZED VIEW MV2");
        } catch (Exception e) {
        }

        try {
            DBConnection.ExecuteUpdate("DROP MATERIALIZED VIEW MV3");
        } catch (Exception e) {
        }

        try {
            DBConnection.ExecuteUpdate("DROP MATERIALIZED VIEW MV4");
        } catch (Exception e) {
        } finally {
            DBConnection.ExecuteUpdate("PURGE RECYCLEBIN");
        }

        ProjectionOperatorNode projectionNode = null;
        StringBuilder s = null;

        /*------MANY JOINS QMV----*/
        projectionNode = ManyJoins(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        MaterializedViewQueryTreeCollection.AddMaterializedViewToMVCollection(new MaterializedViewQueryTree("MV1", projectionNode));
        s = new StringBuilder();
        s.append("CREATE MATERIALIZED VIEW MV1 AS ");
        s.append("SELECT A.SEX, A.AGE_CATEGORY, SUM(1) ");
        s.append("FROM ADMISSIONS A, DIAGNOSIS D, ADMSOURCE_DIM ADS, ");
        s.append("ADMTIME_DIM ADT, ADMTYPE_DIM ATP, AGE_DIM AG, GENDER_DIM G, ");
        s.append("CARE_TYPE_DIM C, SEPARATION_MODE_DIM S, DIAGNOSIS_DIM DM ");
        s.append("WHERE A.PK = D.ADMISSIONS_PK ");
        s.append("AND A.SEX = G.GENDER_NUMBER ");
        s.append("AND A.ADMSOURC = ADS.VAED_VALUE ");
        s.append("AND A.ADMTYPE = ATP.VAED_VALUE ");
        s.append("AND A.CARE = C.VAED_VALUE ");
        s.append("AND A.SEPMODE = S.VAED_VALUE ");
        s.append("AND A.AGE_CATEGORY = AG.AGE_CATEGORY ");
        s.append("AND TO_CHAR(A.ADMDATE, 'YYYYMM') = ADT.ADMTIMEID ");
        s.append("AND SUBSTR(D.DIAG, 1, 1) = SUBSTR(DM.CODE_RANGE_START, 1, 1) ");
        s.append("GROUP BY A.SEX, A.AGE_CATEGORY");
        DBConnection.ExecuteUpdate(s.toString().replaceFirst("ADMISSIONS", "ADMISSIONS" + partition).replaceFirst("DIAGNOSIS", "DIAGNOSIS" + partition));
        /*-----------------------------*/

 /*------MANY GROUP BYS QMV----*/
        projectionNode = ManyGroupBys(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        MaterializedViewQueryTreeCollection.AddMaterializedViewToMVCollection(new MaterializedViewQueryTree("MV2", projectionNode));
        s = new StringBuilder();
        s.append("CREATE MATERIALIZED VIEW MV2 AS ");
        s.append("SELECT A.SEX, A.ADMSOURC, A.ADMTYPE, A.CARE, A.SEPMODE, A.AGE_CATEGORY, ADT.ADMYEAR, ADT.ADMMONTH, SUM(1) ");
        s.append("FROM ADMISSIONS A, ADMTIME_DIM ADT ");
        s.append("WHERE TO_CHAR(A.ADMDATE, 'YYYYMM') = ADT.ADMTIMEID ");
        s.append("GROUP BY A.SEX, A.ADMSOURC, A.ADMTYPE, A.CARE, A.SEPMODE, A.AGE_CATEGORY, ADT.ADMYEAR, ADT.ADMMONTH");
        DBConnection.ExecuteUpdate(s.toString().replaceFirst("ADMISSIONS", "ADMISSIONS" + partition));
        /*-----------------------------*/

 /*------MANY SELECTIONS BYS QMV----*/
        projectionNode = ManySelections(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        MaterializedViewQueryTreeCollection.AddMaterializedViewToMVCollection(new MaterializedViewQueryTree("MV3", projectionNode));
        s = new StringBuilder();
        s.append("CREATE MATERIALIZED VIEW MV3 AS ");
        s.append("SELECT A.AGE_CATEGORY, SUM(1) ");
        s.append("FROM ADMISSIONS A ");
        s.append("WHERE A.ADMSOURC = 'H' ");
        s.append("AND A.ADMTYPE = 'X' ");
        s.append("AND A.CARE = '4' ");
        s.append("AND A.SEPMODE = 'H' ");
        s.append("AND TO_CHAR(A.ADMDATE, 'YYYY') = '2007' ");
        s.append("AND TO_CHAR(A.ADMDATE, 'MM') = '12' ");
        s.append("GROUP BY A.AGE_CATEGORY");
        DBConnection.ExecuteUpdate(s.toString().replaceFirst("ADMISSIONS", "ADMISSIONS" + partition));
        /*-----------------------------*/

 /*------MANY JOINS, GROUP BYS AND SELECTIONS QMV----*/
        projectionNode = ManyJoinsSelectionsGroupBys(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        MaterializedViewQueryTreeCollection.AddMaterializedViewToMVCollection(new MaterializedViewQueryTree("MV4", projectionNode));
        s = new StringBuilder();
        s.append("CREATE MATERIALIZED VIEW MV4 AS ");
        s.append("SELECT A.SEX, A.ADMSOURC, A.ADMTYPE, A.CARE, A.SEPMODE, A.AGE_CATEGORY, ADT.ADMYEAR, ADT.ADMMONTH, SUM(1) ");
        s.append("FROM ADMISSIONS A, DIAGNOSIS D, ADMSOURCE_DIM ADS, ");
        s.append("ADMTIME_DIM ADT, ADMTYPE_DIM ATP, AGE_DIM AG, GENDER_DIM G, ");
        s.append("CARE_TYPE_DIM C, SEPARATION_MODE_DIM S, DIAGNOSIS_DIM DM ");
        s.append("WHERE A.PK = D.ADMISSIONS_PK ");
        s.append("AND A.SEX = G.GENDER_NUMBER ");
        s.append("AND A.ADMSOURC = ADS.VAED_VALUE ");
        s.append("AND A.ADMTYPE = ATP.VAED_VALUE ");
        s.append("AND A.CARE = C.VAED_VALUE ");
        s.append("AND A.SEPMODE = S.VAED_VALUE ");
        s.append("AND A.AGE_CATEGORY = AG.AGE_CATEGORY ");
        s.append("AND A.ADMSOURC = 'H'");
        s.append("AND A.ADMTYPE = 'X' ");
        s.append("AND A.CARE = '4' ");
        s.append("AND A.SEPMODE = 'H' ");
        s.append("AND TO_CHAR(A.ADMDATE, 'YYYYMM') = ADT.ADMTIMEID ");
        s.append("AND SUBSTR(D.DIAG, 1, 1) = SUBSTR(DM.CODE_RANGE_START, 1, 1) ");
        s.append("AND DM.DIAGNOSIS_DESC LIKE 'Diseases%' ");
        s.append("GROUP BY A.SEX, A.ADMSOURC, A.ADMTYPE, A.CARE, A.SEPMODE, A.AGE_CATEGORY, ADT.ADMYEAR, ADT.ADMMONTH");
        DBConnection.ExecuteUpdate(s.toString().replaceFirst("ADMISSIONS", "ADMISSIONS" + partition).replaceFirst("DIAGNOSIS", "DIAGNOSIS" + partition));
        /*-----------------------------*/
    }

    public void addContents(Stage stage) throws Exception {
        Label l1 = new Label("Choose: ");
        ToggleGroup group = new ToggleGroup();
        RadioButton button1 = new RadioButton("IQT 1");
        button1.setToggleGroup(group);
        RadioButton button2 = new RadioButton("IQT 2");
        button2.setToggleGroup(group);
        RadioButton button3 = new RadioButton("IQT 3");
        button3.setToggleGroup(group);
        RadioButton button4 = new RadioButton("IQT 4");
        button4.setToggleGroup(group);

        button1.setSelected(true);

        ArrayList<String> partitions = new ArrayList<String>();
        // This is not considered as it's the initial.
        // After this the program seems to perform some sort of caching
        partitions.add("_100");

        partitions.add("_100");
        partitions.add("_200");
        partitions.add("_300");
        partitions.add("_400");
        partitions.add("_500");

        partitions.add("_600");
        partitions.add("_700");
        partitions.add("_800");
        partitions.add("_900");
        partitions.add("_1000");

        partitions.add("_1100");
        partitions.add("_1200");
        partitions.add("_1300");
        partitions.add("_1400");
        partitions.add("_1500");

        partitions.add("_1600");
        partitions.add("_1700");
        partitions.add("_1800");
        partitions.add("_1900");
        partitions.add("_2000");

        partitions.add("_3000");
        partitions.add("_4000");
        partitions.add("_5000");

        partitions.add("_6000");
        partitions.add("_7000");
        partitions.add("_8000");
        partitions.add("_9000");
        partitions.add("_10000");

        partitions.add("_11000");
        partitions.add("_12000");
        partitions.add("_13000");
        partitions.add("_14000");
        partitions.add("_15000");

        partitions.add("_16000");
        partitions.add("_17000");
        partitions.add("_18000");
        partitions.add("_19000");
        partitions.add("_20000");

        partitions.add("_30000");
        partitions.add("_40000");
        partitions.add("_50000");

        partitions.add("_60000");
        partitions.add("_70000");
        partitions.add("_80000");
        partitions.add("_90000");
        partitions.add("_100000");

        partitions.add("_110000");
        partitions.add("_120000");
        partitions.add("_130000");
        partitions.add("_140000");
        partitions.add("_150000");

        partitions.add("_160000");
        partitions.add("_170000");
        partitions.add("_180000");
        partitions.add("_190000");
        partitions.add("_200000");

        partitions.add("_300000");
        partitions.add("_400000");
        partitions.add("_500000");

        partitions.add("_600000");
        partitions.add("_700000");
        partitions.add("_800000");
        partitions.add("_900000");
        partitions.add("_1000000");

        partitions.add("_1100000");
        partitions.add("_1200000");
        partitions.add("_1300000");
        partitions.add("_1400000");
        partitions.add("_1500000");

        Button Calculate = new Button("Calculate");
        Calculate.setOnAction((e) -> {
            PrintWriter printToFile = null;
            try {
                printToFile = new PrintWriter(new File("Query Matching with index.txt"));

                for (String partition : partitions) {
                    double totaltime = 0;

                    System.out.println("Partition: " + partition);
                    Admissions = "Admissions" + partition;
                    Diagnosis = "Diagnosis" + partition;

                    TableNode ADMISSIONS_NODE = new TableNode(Admissions, TableTypeEnum.FACT, "A", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category");
                    TableNode DIAGNOSIS_NODE = new TableNode(Diagnosis, TableTypeEnum.DIMENSION, "D", "admissions_pk", "position", "diag", "pref");

                    InputQueryTree qt = null;

                    if (group.getSelectedToggle().equals(button1)) {
                        //Aimed at retrievinng MV1                    
                        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
                        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
                        SelectionCondition selectionCondition = new SelectionCondition(
                                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO)); // extra condition hence result is subset of MV1
                        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
                        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY");
                        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
                        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
                        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY");
                        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
                        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

                        String sql = "SELECT A.SEX, A.AGE_CATEGORY, SUM(1) "
                                + "FROM ADMISSIONS" + partition + " A, DIAGNOSIS" + partition + " D, ADMSOURCE_DIM ADS, "
                                + "ADMTIME_DIM ADT, ADMTYPE_DIM ATP, AGE_DIM AG, GENDER_DIM G, "
                                + "CARE_TYPE_DIM C, SEPARATION_MODE_DIM S, DIAGNOSIS_DIM DM "
                                + "WHERE A.PK = D.ADMISSIONS_PK "
                                + "AND A.SEX = G.GENDER_NUMBER "
                                + "AND A.ADMSOURC = ADS.VAED_VALUE "
                                + "AND A.ADMTYPE = ATP.VAED_VALUE "
                                + "AND A.CARE = C.VAED_VALUE "
                                + "AND A.SEPMODE = S.VAED_VALUE "
                                + "AND A.AGE_CATEGORY = AG.AGE_CATEGORY "
                                + "AND TO_CHAR(A.ADMDATE, 'YYYYMM') = ADT.ADMTIMEID "
                                + "AND SUBSTR(D.DIAG, 1, 1) = SUBSTR(DM.CODE_RANGE_START, 1, 1) "
                                + "AND A.ADMSOURC = 'H' "
                                + "GROUP BY A.SEX, A.AGE_CATEGORY";
                        qt = new InputQueryTree(sql, projectionNode);
                    }

                    if (group.getSelectedToggle().equals(button2)) {
                        //Aimed at retrievinng MV2
                        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, ADMTIME_DIM_NODE);
                        SelectionCondition selectionCondition = new SelectionCondition(
                                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO));
                        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin1);
                        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR");
                        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
                        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
                        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR");
                        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
                        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

                        String sql = "SELECT A.SEX, A.ADMSOURC, A.ADMTYPE, A.CARE, A.SEPMODE, A.AGE_CATEGORY, ADT.ADMYEAR, SUM(1) "
                                + "FROM ADMISSIONS" + partition + " A, ADMTIME_DIM ADT "
                                + "WHERE TO_CHAR(A.ADMDATE, 'YYYYMM') = ADT.ADMTIMEID "
                                + "GROUP BY A.SEX, A.ADMSOURC, A.ADMTYPE, A.CARE, A.SEPMODE, A.AGE_CATEGORY, ADT.ADMYEAR";
                        qt = new InputQueryTree(sql, projectionNode);
                    }

                    if (group.getSelectedToggle().equals(button3)) {
                        SelectionCondition selectionCondition = new SelectionCondition(
                                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("TO_CHAR(A.ADMDATE, 'YYYY')", "'2007'", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("TO_CHAR(A.ADMDATE, 'mm')", "'12'", RelationalOperatorEnum.EQUAL_TO));
                        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, ADMISSIONS_NODE);
                        GroupByCondition groupByCondition = new GroupByCondition("A.AGE_CATEGORY");
                        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
                        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
                        ProjectionCondition projectionCondition = new ProjectionCondition("A.AGE_CATEGORY");
                        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
                        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

                        String sql = "SELECT A.AGE_CATEGORY, SUM(1) "
                                + "FROM ADMISSIONS" + partition + " A "
                                + "WHERE A.ADMSOURC = 'H' "
                                + "AND A.ADMTYPE = 'X' "
                                + "AND A.CARE = '4' "
                                + "AND A.SEPMODE = 'H' "
                                + "AND TO_CHAR(A.ADMDATE, 'YYYY') = '2007' "
                                + "AND TO_CHAR(A.ADMDATE, 'MM') = '12' "
                                + "AND TO_CHAR(A.ADMDATE, 'DD') = '10' "
                                + "GROUP BY A.AGE_CATEGORY";
                        qt = new InputQueryTree(sql, projectionNode);
                    }

                    if (group.getSelectedToggle().equals(button4)) {
                        //Aimed at retrievinng MV3
                        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
                        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
                        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
                        SelectionCondition selectionCondition = new SelectionCondition(
                                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
                        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
                        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY");
                        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
                        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
                        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY");
                        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
                        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

                        String sql = "SELECT A.SEX, A.ADMSOURC, A.ADMTYPE, A.CARE, A.SEPMODE, A.AGE_CATEGORY, SUM(1) "
                                + "FROM ADMISSIONS" + partition + " A, DIAGNOSIS" + partition + " D, ADMSOURCE_DIM ADS, "
                                + "ADMTIME_DIM ADT, ADMTYPE_DIM ATP, AGE_DIM AG, GENDER_DIM G, "
                                + "CARE_TYPE_DIM C, SEPARATION_MODE_DIM S, DIAGNOSIS_DIM DM "
                                + "WHERE A.PK = D.ADMISSIONS_PK "
                                + "AND A.SEX = G.GENDER_NUMBER "
                                + "AND A.ADMSOURC = ADS.VAED_VALUE "
                                + "AND A.ADMTYPE = ATP.VAED_VALUE "
                                + "AND A.CARE = C.VAED_VALUE "
                                + "AND A.SEPMODE = S.VAED_VALUE "
                                + "AND A.AGE_CATEGORY = AG.AGE_CATEGORY "
                                + "AND A.ADMSOURC = 'H' "
                                + "AND A.ADMTYPE = 'X' "
                                + "AND A.CARE = '4' "
                                + "AND A.SEPMODE = 'H' "
                                + "AND TO_CHAR(A.ADMDATE, 'YYYYMM') = ADT.ADMTIMEID "
                                + "AND SUBSTR(D.DIAG, 1, 1) = SUBSTR(DM.CODE_RANGE_START, 1, 1) "
                                + "AND DM.DIAGNOSIS_DESC LIKE 'Diseases%' "
                                + "GROUP BY A.SEX, A.ADMSOURC, A.ADMTYPE, A.CARE, A.SEPMODE, A.AGE_CATEGORY";
                        qt = new InputQueryTree(sql, projectionNode);
                    }

                    // Populate B+ tree
                    InitializeMVCollection(partition, ADMISSIONS_NODE, DIAGNOSIS_NODE);

                    // repeat 15 times
                    for (int i = 0; i < 15; ++i) {
                        long timeTaken = 0;

                        // Time taken to perform MV-to-qery matching
                        long start = System.nanoTime();
                        ArrayList<MaterializedViewQueryTree> result = MaterializedViewQueryTreeCollection.FindMatch(qt);
                        long end = System.nanoTime();

                        timeTaken += (end - start);

                        if (result.isEmpty()) {
                            if (i == 0) {
                                System.out.println("No match found");
                            }
                        } else {
                            if (i == 0) {
                                System.out.println("Matching materialized view found: " + result.get(0));
                            }

                            // Time taken to retrieve the MV result
                            start = System.nanoTime();
                            ResultSet rs = DBConnection.ExecuteQuery("select * from " + result.get(0).name);
                            while (rs.next()) {
                                rs.toString();
                            }
                            end = System.nanoTime();

                            rs.close();

                            timeTaken += (end - start);

                            // Only sum up the last 10
                            if (i >= 5) {
                                totaltime += timeTaken;
                            }
                        }
                    }

                    totaltime = totaltime / 10;
                    System.out.println("Total Time Taken with MV:" + totaltime + "\n");
                    printToFile.print(totaltime + "\t");

                    totaltime = 0;
                    for (int i = 0; i < 15; ++i) {
                        long timeTaken = 0;

                        // Time taken to run query directly on DW
                        long start = System.nanoTime();
                        ResultSet rs = DBConnection.ExecuteQuery(qt.getSql());
                        while (rs.next()) {
                            rs.toString();
                        }
                        long end = System.nanoTime();
                        rs.close();

                        timeTaken += end - start;

                        // Only sum up the last 10
                        if (i >= 5) {
                            totaltime += timeTaken;
                        }
                    }

                    totaltime = totaltime / 10;
                    System.out.println("Total Time Taken for direct run:" + totaltime + "\n");
                    printToFile.print(totaltime + "\n");

                    DBConnection.Disconnect();
                }
            } catch (Exception ex) {
                Logger.getLogger(Experimenter_Query_Matching.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                printToFile.flush();
                printToFile.close();
            }

            System.out.println("Calculations are complete");
        });

        Button MVSizes = new Button("MV Sizes");
        MVSizes.setOnAction((e) -> {
            try {
                PrintWriter printToFile = new PrintWriter(new File("MV sizes.txt"));

                for (String partition : partitions) {
                    System.out.println("Partition: " + partition);

                    String sql = "SELECT A.SEX, A.ADMSOURC, A.ADMTYPE, A.CARE, A.SEPMODE, A.AGE_CATEGORY, ADT.ADMYEAR, SUM(1) "
                            + "FROM ADMISSIONS" + partition + " A, ADMTIME_DIM ADT "
                            + "WHERE TO_CHAR(A.ADMDATE, 'YYYYMM') = ADT.ADMTIMEID "
                            + "GROUP BY A.SEX, A.ADMSOURC, A.ADMTYPE, A.CARE, A.SEPMODE, A.AGE_CATEGORY, ADT.ADMYEAR";

                    ResultSet rs = DBConnection.ExecuteQuery(sql);
                    int count = 0;
                    while (rs.next()) {
                        rs.toString();
                        ++count;
                    }
                    rs.close();

                    printToFile.print(count + "\n");

                    DBConnection.Disconnect();
                }

                printToFile.flush();
                printToFile.close();
            } catch (Exception ex) {
                Logger.getLogger(Experimenter_Query_Matching.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.println("Done");
        });

        HBox executeHBox = new HBox(Calculate, MVSizes);
        executeHBox.setAlignment(Pos.CENTER);
        executeHBox.setSpacing(10);

        HBox top = new HBox(l1, button1, button2, button3, button4);
        top.setAlignment(Pos.CENTER);
        HBox bottom = new HBox();
        top.minHeight(10);
        bottom.minHeight(10);

        VBox vbox = new VBox(top, executeHBox, bottom);

        vbox.setSpacing(10);
        vbox.setStyle("-fx-background-color: wheat; -fx-font-size: 20");
        vbox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vbox, 500, 300);
        stage.setScene(scene);
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
