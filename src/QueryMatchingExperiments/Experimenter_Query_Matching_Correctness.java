package QueryMatchingExperiments;

import querytree.structure.conditions.AggregateFunction;
import querytree.structure.conditions.Statement;
import querytree.structure.conditions.ProjectionCondition;
import querytree.structure.conditions.SelectionCondition;
import querytree.structure.conditions.GroupByCondition;
import querytree.structure.SelectionOperatorNode;
import querytree.structure.ProjectionOperatorNode;
import querytree.structure.GroupByOperatorNode;
import querytree.structure.TableNode;
import querytree.structure.CartesianProductOperatorNode;
import querytree.enums.AggregateFunctionEnum;
import querytree.enums.TableTypeEnum;
import querytree.enums.RelationalOperatorEnum;
import querytree.InputQueryTree;
import querytree.MaterializedViewQueryTree;
import java.io.File;
import java.io.PrintWriter;
import querytree.utilities.DBConnection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Experimenter_Query_Matching_Correctness extends Application {

    private static final TableNode ADMISSIONS_NODE = new TableNode("ADMISSIONS", TableTypeEnum.FACT, "A", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category");
    private static final TableNode DIAGNOSIS_NODE = new TableNode("DIAGNOSIS", TableTypeEnum.DIMENSION, "D", "admissions_pk", "position", "diag", "pref");

    private static final TableNode DIAGNOSIS_DIM_NODE = new TableNode("DIAGNOSIS_DIM", TableTypeEnum.DIMENSION, "DM", "diagnosis_num", "diagnosis_desc", "code_range_start", "code_range_end");
    private static final TableNode ADMSOURCE_DIM_NODE = new TableNode("ADMSOURCE_DIM", TableTypeEnum.DIMENSION, "ADS", "vaed_value", "cd_desc");
    private static final TableNode ADMTYPE_DIM_NODE = new TableNode("ADMTYPE_DIM", TableTypeEnum.DIMENSION, "ATP", "vaed_value", "cd_desc");
    private static final TableNode CARE_TYPE_DIM_NODE = new TableNode("CARE_TYPE_DIM", TableTypeEnum.DIMENSION, "C", "vaed_value", "cd_desc");
    private static final TableNode SEPARATION_MODE_DIM_NODE = new TableNode("SEPARATION_MODE_DIM", TableTypeEnum.DIMENSION, "S", "vaed_value", "cd_desc");
    private static final TableNode ADMTIME_DIM_NODE = new TableNode("ADMTIME_DIM", TableTypeEnum.DIMENSION, "ADT", "admtimeid", "admyear", "admmonth");
    private static final TableNode AGE_DIM_NODE = new TableNode("AGE_DIM", TableTypeEnum.DIMENSION, "AG", "age_category", "age_desc", "start_age", "end_age");
    private static final TableNode GENDER_DIM_NODE = new TableNode("GENDER_DIM", TableTypeEnum.DIMENSION, "G", "gender_number", "gender_desc");

    @Override
    public void start(Stage stage) throws Exception {
        addContents(stage);
        stage.setResizable(true);
        stage.setTitle(getClass().getName());
        stage.show();
    }

    public static ProjectionOperatorNode ManyJoins(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode ManyGroupBys(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, ADMTIME_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin1);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode ManySelections(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYY')", "'2007'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'mm')", "'12'", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, ADMISSIONS_NODE);
        GroupByCondition groupByCondition = new GroupByCondition("A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode ManyJoinsSelectionsGroupBys(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode SpecialFunctionInSelection(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin2);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode To_CharInGroupBy(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        GroupByCondition groupByCondition = new GroupByCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, ADMISSIONS_NODE);
        ProjectionCondition projectionCondition = new ProjectionCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    private void InitializeMVCollection() throws Exception {
        MaterializedViewQueryTreeCollection.Init();

        ProjectionOperatorNode projectionNode = null;

        projectionNode = ManyJoins(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        MaterializedViewQueryTreeCollection.AddMaterializedViewToMVCollection(new MaterializedViewQueryTree("MV1", projectionNode));
        /*-----------------------------*/

        projectionNode = ManyGroupBys(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        MaterializedViewQueryTreeCollection.AddMaterializedViewToMVCollection(new MaterializedViewQueryTree("MV2", projectionNode));
        /*-----------------------------*/

        projectionNode = ManySelections(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        MaterializedViewQueryTreeCollection.AddMaterializedViewToMVCollection(new MaterializedViewQueryTree("MV3", projectionNode));
        /*-----------------------------*/

        projectionNode = ManyJoinsSelectionsGroupBys(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        MaterializedViewQueryTreeCollection.AddMaterializedViewToMVCollection(new MaterializedViewQueryTree("MV4", projectionNode));
        /*-----------------------------*/

        projectionNode = SpecialFunctionInSelection(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        MaterializedViewQueryTreeCollection.AddMaterializedViewToMVCollection(new MaterializedViewQueryTree("MV4", projectionNode));
        /*-----------------------------*/

        projectionNode = To_CharInGroupBy(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        MaterializedViewQueryTreeCollection.AddMaterializedViewToMVCollection(new MaterializedViewQueryTree("MV4", projectionNode));
        /*-----------------------------*/
    }

    public void addContents(Stage stage) throws Exception {
        Label l1 = new Label("Choose: ");
        ToggleGroup group = new ToggleGroup();
        RadioButton button1 = new RadioButton("IQT 1");
        button1.setToggleGroup(group);
        RadioButton button2 = new RadioButton("IQT 2");
        button2.setToggleGroup(group);
        RadioButton button3 = new RadioButton("IQT 3");
        button3.setToggleGroup(group);
        RadioButton button4 = new RadioButton("IQT 4");
        button4.setToggleGroup(group);

        button1.setSelected(true);

        Button FindMatch = new Button("Find match");
        FindMatch.setOnAction((e) -> {
            try {
                // Populate B+ tree
                InitializeMVCollection();
                InputQueryTree qt = null;

                if (group.getSelectedToggle().equals(button1)) {
                    GroupByCondition groupByCondition = new GroupByCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
                    groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
                    GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, ADMISSIONS_NODE);
                    ProjectionCondition projectionCondition = new ProjectionCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')", "TO_CHAR(ADMDATE, 'DD')");
                    projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
                    ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

                    qt = new InputQueryTree(projectionNode);
                }

                if (group.getSelectedToggle().equals(button2)) {
                    //Aimed at retrievinng MV2
                    CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
                    CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
                    SelectionCondition selectionCondition = new SelectionCondition(
                            new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO));
                    SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
                    GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY", "A.SEPMODE");
                    groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
                    GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
                    ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY", "A.SEPMODE");
                    projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
                    ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

                    qt = new InputQueryTree(projectionNode);
                }

                if (group.getSelectedToggle().equals(button3)) {
                    CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
                    CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
                    SelectionCondition selectionCondition = new SelectionCondition(
                            new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO));
                    SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
                    GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY");
                    groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
                    GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
                    ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY");
                    projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
                    ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

                    qt = new InputQueryTree(projectionNode);
                }

                if (group.getSelectedToggle().equals(button4)) {
                    //Aimed at retrievinng MV3
                    CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
                    CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
                    CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
                    SelectionCondition selectionCondition = new SelectionCondition(
                            new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                            new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO));
                    SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
                    GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY");
                    groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
                    GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
                    ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY");
                    projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
                    ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

                    qt = new InputQueryTree(projectionNode);
                }

                // repeat 15 times
                // Time taken to perform MV-to-qery matching
                ArrayList<MaterializedViewQueryTree> result = MaterializedViewQueryTreeCollection.FindMatch(qt);

                if (result.isEmpty()) {
                    System.out.println("No match found");
                } else {
                    System.out.println("Matching materialized view found: " + result.get(0));
                }
            } catch (Exception ex) {

            }
        });

        HBox executeHBox = new HBox(FindMatch);
        executeHBox.setAlignment(Pos.CENTER);
        executeHBox.setSpacing(10);

        HBox top = new HBox(l1, button1, button2, button3, button4);

        top.setAlignment(Pos.CENTER);
        HBox bottom = new HBox();

        top.minHeight(10);
        bottom.minHeight(10);

        VBox vbox = new VBox(top, executeHBox, bottom);

        vbox.setSpacing(10);
        vbox.setStyle("-fx-background-color: wheat; -fx-font-size: 20");
        vbox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vbox, 500, 300);

        stage.setScene(scene);
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
