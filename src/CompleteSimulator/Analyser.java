package CompleteSimulator;

import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import querytree.MaterializedViewQueryTree;
import querytree.enums.StatusEnum;
import querytree.utilities.DBConnection;

public class Analyser extends Thread {

    public static int SESSION_LENGTH = 30000; // milliseconds
    public static int SESSION_THRESHOLD = 3;

    @Override
    public void run() {
        int currentSessionId = MaterializedViewQueryTreeCollection.sessionId;

        MaterializedViewQueryTreeCollection.ResetCurrentSession();

        if (currentSessionId == SESSION_THRESHOLD) {

            MaterializedViewQueryTreeCollection.sessionId = 1;

            ArrayList<MaterializedViewQueryTree> promote = new ArrayList<MaterializedViewQueryTree>();
            ArrayList<MaterializedViewQueryTree> demote = new ArrayList<MaterializedViewQueryTree>();
            ArrayList<MaterializedViewQueryTree> delete = new ArrayList<MaterializedViewQueryTree>();

            MaterializedViewQueryTreeCollection.collection_id_as_key.forEach((k, v) -> {
                try (ResultSet rs = DBConnection.ExecuteQuery("SELECT * FROM " + MaterializedViewQueryTreeCollection.QUERY_SESSIONS_TABLE + " WHERE QUERYTREEID=" + k)) {
                    int sum_of_hits = 0;

                    while (rs.next()) {
                        sum_of_hits += rs.getInt(3);
                    }

                    if (sum_of_hits == 0) {
                        demote.add(v);

                    }
                } catch (Exception ex) {
                    Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
            });

            MaterializedViewQueryTreeCollection.secondaryCollection.forEach((k, v) -> {
                try (ResultSet rs = DBConnection.ExecuteQuery("SELECT * FROM " + MaterializedViewQueryTreeCollection.QUERY_SESSIONS_TABLE + " WHERE QUERYTREEID=" + k)) {
                    int count = 0;

                    while (rs.next()) {
                        if (rs.getInt(3) >= 1) {
                            ++count;
                        }
                    }

                    if (count == SESSION_THRESHOLD) {
                        promote.add(v);

                    } else if (v.status == StatusEnum.SOFT_DELETED && count == 0) {
                        delete.add(v);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
            });

            for (MaterializedViewQueryTree mv : promote) {
                try {
                    MaterializedViewQueryTreeCollection.ReActivateMaterializedView(mv);
                    System.out.println(mv.toString() + " promoted to primary b+ tree");
                } catch (Exception ex) {
                    Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            for (MaterializedViewQueryTree mv : demote) {
                try {
                    MaterializedViewQueryTreeCollection.SoftDeleteMaterializedView(mv);
                    System.out.println(mv.toString() + " was soft deleted");
                } catch (Exception ex) {
                    Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            for (MaterializedViewQueryTree mv : delete) {
                MaterializedViewQueryTreeCollection.DeletePermanentlyMaterializedView(mv);
                System.out.println(mv.toString() + " was deleted permanently");
            }

            try {
                MaterializedViewQueryTreeCollection.CreateOrTruncateTable();
            } catch (SQLException ex) {
                Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
            }

            /*
            StringWriter out = new StringWriter();
            MaterializedViewQueryTreeCollection.primaryCollection.printXml(out);
            System.out.println("Primary :\n" + out);

            out = new StringWriter();
            MaterializedViewQueryTreeCollection.secondaryCollection.printXml(out);
            System.out.println("Secondary :\n" + out);
             */
        }
    }
}
