package CompleteSimulator;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import querytree.utilities.DBConnection;

public final class DWChangeTracker extends Thread {

    public static HashMap<String, TableStats> tables;
    public static String table_rows = "SELECT COUNT(*) FROM ";
    public static String table_columns = "SELECT COLUMN_NAME FROM USER_TAB_COLUMNS WHERE TABLE_NAME = ";
    public static String[] tableNames;
    public static int TIME_INTERVAL = 30000;

    @Override
    public void run() {
        try {
            if (DWChangeTracker.tables != null) {
                boolean changed = HasChangedSinceLastCheck();
                System.out.println(DWChangeTracker.tables);
                System.out.println("DW has changed: " + changed);
            }
        } catch (Exception ex) {
            Logger.getLogger(DWChangeTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean HasChangedSinceLastCheck() throws Exception {
        HashMap<String, TableStats> newTableInformation = GetTableInformation();
        boolean result = false;

        for (Map.Entry<String, TableStats> entry : tables.entrySet()) {
            String key = entry.getKey();
            TableStats value = entry.getValue();

            TableStats tableStats = newTableInformation.get(key);

            if (tableStats == null || !value.equals(tableStats)) {
                result = true;
                break;
            }
        }

        tables = newTableInformation;

        return result;
    }

    public static HashMap<String, TableStats> GetTableInformation() throws Exception {
        HashMap<String, TableStats> temp = new HashMap<String, TableStats>();

        for (String table : DWChangeTracker.tableNames) {
            int num_rows = 0;
            ArrayList<String> columns = new ArrayList<String>();
            StringBuilder sql = new StringBuilder(table_rows);
            sql.append(table.toUpperCase());

            ResultSet rs = DBConnection.ExecuteQuery(sql.toString());

            if (rs.next()) {
                num_rows = rs.getInt(1);
            } else {
                throw new Exception("Table not found 1 ");
            }

            sql = new StringBuilder(table_columns);
            sql.append("'").append(table.toUpperCase()).append("'");
            rs = DBConnection.ExecuteQuery(sql.toString());

            while (rs.next()) {
                columns.add(rs.getString(1));
            }

            if (columns.isEmpty()) {
                throw new Exception("Table not found 2 ");
            }

            temp.put(table, new TableStats(table, num_rows, columns.toArray(new String[0])));
        }

        return temp;
    }
}
