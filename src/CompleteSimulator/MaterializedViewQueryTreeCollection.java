package CompleteSimulator;

import querytree.InputQueryTree;
import querytree.QueryMatching;
import querytree.MaterializedViewQueryTree;
import querytree.utilities.DBConnection;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import querytree.enums.StatusEnum;

public final class MaterializedViewQueryTreeCollection {

    public static HashMap<String, ArrayList<MaterializedViewQueryTree>> primaryCollection;
    public static HashMap<Integer, MaterializedViewQueryTree> collection_id_as_key;
    public static HashMap<Integer, MaterializedViewQueryTree> secondaryCollection;
    public static int sessionId = 0;
    public static final String QUERY_SESSIONS_TABLE = "QUERYTREE_SESSION_HITS";

    public static void Init() throws SQLException {
        // B+ tree with table names as keys
        // The value of each key is a list of query trees that have that tree
        primaryCollection = new HashMap<String, ArrayList<MaterializedViewQueryTree>>();

        // B+ tree with query tree id as key where each query tree 
        // The value is the query tree
        collection_id_as_key = new HashMap<Integer, MaterializedViewQueryTree>();

        // Store soft-deleted query trees here
        secondaryCollection = new HashMap<Integer, MaterializedViewQueryTree>();

        sessionId = 1;

        CreateOrTruncateTable();
    }

    public static ArrayList<MaterializedViewQueryTree> FindMatch(InputQueryTree input) throws Exception {
        ArrayList<MaterializedViewQueryTree> result = new ArrayList<MaterializedViewQueryTree>();
        ArrayList<MaterializedViewQueryTree> materializedViewQueryTrees = new ArrayList<MaterializedViewQueryTree>();
        ArrayList<String> tableNames = input.root.GetTableNames();
        HashMap<String, ArrayList<MaterializedViewQueryTree>> trees = new HashMap<String, ArrayList<MaterializedViewQueryTree>>();
        HashMap<MaterializedViewQueryTree, Integer> counts = new HashMap<MaterializedViewQueryTree, Integer>();

        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> value = primaryCollection.get(table);
            if (value != null) {
                trees.put(table, value);
            } else {
                trees.put(table, new ArrayList<MaterializedViewQueryTree>());
            }
        }

        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> temp = trees.get(table);
            for (MaterializedViewQueryTree q : temp) {
                Integer i = counts.get(q);
                if (i == null) {
                    counts.put(q, 1);
                } else {
                    counts.replace(q, ++i);
                }
            }
        }

        Set<MaterializedViewQueryTree> keyset = counts.keySet();
        for (MaterializedViewQueryTree q : keyset) {
            Integer i = counts.get(q);
            if (i == tableNames.size()) {
                materializedViewQueryTrees.add(q);
            }
        }

        for (MaterializedViewQueryTree mv : materializedViewQueryTrees) {
            double score = new QueryMatching(mv, input).Calculate();

            if (score == 4) {
                mv.currentSessionHits++;
                result.add(mv);
            }
        }

        if (result.isEmpty()) {
            secondaryCollection.forEach((k, v) -> {
                double score;
                try {
                    score = new QueryMatching(v, input).Calculate();
                    if (score == 4) {
                        // System.out.println("Matching query found in secondary storage");
                        v.currentSessionHits++;
                        result.add(v);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(MaterializedViewQueryTreeCollection.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }

        if (result.isEmpty()) {
            MaterializedViewQueryTree mv = new MaterializedViewQueryTree(input.root);
            mv.status = StatusEnum.INACTIVE;
            secondaryCollection.put(mv.id, mv);
        }

        return result;
    }

    public static void AddMaterializedViewToPrimary(MaterializedViewQueryTree tree) throws Exception {
        AddMaterializedViewToPrimaryCollection(tree);
        collection_id_as_key.putIfAbsent(tree.id, tree);

        System.out.println("MV query tree added to primary b+ tree");
    }

    private static void AddMaterializedViewToPrimaryCollection(MaterializedViewQueryTree tree) throws Exception {
        tree.status = StatusEnum.ACTIVE;
        ArrayList<String> tableNames = tree.root.GetTableNames();
        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> value = primaryCollection.get(table);
            if (value != null) {
                if (!value.contains(tree)) {
                    value.add(tree);
                }
            } else {
                value = new ArrayList<>();
                value.add(tree);
                primaryCollection.put(table, value);
            }
        }
    }

    public static void SoftDeleteMaterializedView(MaterializedViewQueryTree tree) throws Exception {
        tree.status = StatusEnum.SOFT_DELETED;
        secondaryCollection.put(tree.id, tree);

        ArrayList<String> tableNames = tree.root.GetTableNames();

        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> value = primaryCollection.get(table);
            value.remove(tree);
        }

        collection_id_as_key.remove(tree.id);
    }

    public static void DeletePermanentlyMaterializedView(MaterializedViewQueryTree tree) {
        secondaryCollection.remove(tree.id);
    }

    public static void ReActivateMaterializedView(MaterializedViewQueryTree tree) throws Exception {
        AddMaterializedViewToPrimary(tree);
        secondaryCollection.remove(tree.id);
    }

    public static void CreateOrTruncateTable() throws SQLException {
        try {
            DBConnection.ExecuteQuery("TRUNCATE TABLE " + QUERY_SESSIONS_TABLE);
            System.out.println("Table Truncated");
        } catch (Exception e) {
            StringBuilder sql = new StringBuilder();
            sql.append("CREATE TABLE QUERYTREE_SESSION_HITS(");
            sql.append("QUERYTREEID NUMBER NOT NULL");
            sql.append(",SESSIONID NUMBER NOT NULL");
            sql.append(",HITS NUMBER NOT NULL");
            sql.append(",CONSTRAINT QUERYTREE_SESSION_HITS_PK PRIMARY KEY");
            sql.append("(QUERYTREEID,SESSIONID))");
            DBConnection.ExecuteQuery(sql.toString());
            System.out.println("Table created");
        }
    }

    public static void ResetCurrentSession() {
        collection_id_as_key.forEach((k, v) -> {
            try {
                InsertToLog(k, v);
                ((MaterializedViewQueryTree) v).ResetHits();
            } catch (SQLException ex) {
                Logger.getLogger(MaterializedViewQueryTreeCollection.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        secondaryCollection.forEach((k, v) -> {
            try {
                InsertToLog(k, v);
                ((MaterializedViewQueryTree) v).ResetHits();
            } catch (SQLException ex) {
                Logger.getLogger(MaterializedViewQueryTreeCollection.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        sessionId++;

        System.out.println("Reset session complete");
    }

    private static void InsertToLog(int key, MaterializedViewQueryTree mvqt) throws SQLException {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ").append(QUERY_SESSIONS_TABLE).append(" VALUES (").append(key).append(",").append(sessionId).append(",").append(mvqt.currentSessionHits).append(")");
        System.out.println(sql.toString());
        DBConnection.ExecuteUpdate(sql.toString());
    }
}
