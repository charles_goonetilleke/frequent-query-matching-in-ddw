package CompleteSimulator;

import querytree.structure.conditions.AggregateFunction;
import querytree.structure.conditions.Statement;
import querytree.structure.conditions.ProjectionCondition;
import querytree.structure.conditions.SelectionCondition;
import querytree.structure.conditions.GroupByCondition;
import querytree.structure.SelectionOperatorNode;
import querytree.structure.ProjectionOperatorNode;
import querytree.structure.GroupByOperatorNode;
import querytree.structure.TableNode;
import querytree.structure.CartesianProductOperatorNode;
import querytree.enums.AggregateFunctionEnum;
import querytree.enums.TableTypeEnum;
import querytree.enums.RelationalOperatorEnum;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import querytree.utilities.DBConnection;
import querytree.InputQueryTree;
import querytree.MaterializedViewQueryTree;

public class Simulator extends Application {

    private Timer analyserTimer;
    private Timer dwChangeTrackerTimer;

    @Override
    public void start(Stage stage) throws Exception {
        InitializeSystem();
        addContents(stage);
        stage.setResizable(true);
        stage.setMaximized(true);
        stage.setTitle(getClass().getName());
        stage.show();

        //Analyser code
        this.analyserTimer = new Timer();
        analyserTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                new Analyser().start();
            }
        }, Analyser.SESSION_LENGTH, Analyser.SESSION_LENGTH);
         
    }

    public void InitializeSystem() throws SQLException, Exception {
        MaterializedViewQueryTreeCollection.Init();
        this.InitializeMVCollection();
    }

    private void InitializeTableData(ObservableList<InputQueryTree> tableData) throws Exception {
        GroupByCondition groupByCondition = new GroupByCondition("TO_CHAR(ADMDATE, 'YYYY')");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));

        GroupByOperatorNode groupByNode = new GroupByOperatorNode(
                groupByCondition,
                new TableNode("Admissions_5000", TableTypeEnum.FACT, "a", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category"));

        ProjectionCondition projectionCondition = new ProjectionCondition("TO_CHAR(ADMDATE, 'YYYY')");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

        String sql = "SELECT TO_CHAR(ADMDATE, 'YYYY'), SUM(1) FROM ADMISSIONS_5000 GROUP BY TO_CHAR(ADMDATE, 'YYYY')";
        tableData.add(new InputQueryTree(sql, projectionNode));
        /*-----------------------------*/

        groupByCondition = new GroupByCondition("TO_CHAR(ADMDATE, 'MM')");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));

        groupByNode = new GroupByOperatorNode(
                groupByCondition,
                new TableNode("Admissions_5000", TableTypeEnum.FACT, "a", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category"));

        projectionCondition = new ProjectionCondition("TO_CHAR(ADMDATE, 'MM')");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

        sql = "SELECT TO_CHAR(ADMDATE, 'MM'), SUM(1) FROM ADMISSIONS_5000 GROUP BY TO_CHAR(ADMDATE, 'MM')";
        tableData.add(new InputQueryTree(sql, projectionNode));
        /*-----------------------------*/

        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(
                new TableNode("ADMISSIONS_5000", TableTypeEnum.FACT, "A", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category"),
                new TableNode("DIAGNOSIS_5000", TableTypeEnum.DIMENSION, "D", "admissions_pk", "position", "diag", "pref"));

        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(
                cartesianJoin1,
                new TableNode("DIAGNOSIS_DIM", TableTypeEnum.DIMENSION, "DM", "diagnosis_num", "diagnosis_desc", "code_range_start", "code_range_end"));

        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE),
                new Statement("PREF", "P", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin2);

        groupByCondition = new GroupByCondition("sex");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));

        groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);

        projectionCondition = new ProjectionCondition("sex");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

        sql = "SELECT SEX, SUM(1) FROM ADMISSIONS_5000 A, DIAGNOSIS_5000 D, DIAGNOSIS_DIM DM WHERE A.PK = D.ADMISSIONS_PK AND SUBSTR(D.DIAG, 1, 1) = SUBSTR(DM.CODE_RANGE_START, 1, 1) AND DM.DIAGNOSIS_DESC LIKE 'Diseases%' AND PREF = 'P' GROUP BY SEX";
        tableData.add(new InputQueryTree(sql, projectionNode));
        /*-----------------------------*/
    }

    private void InitializeMVCollection() throws Exception {
        GroupByCondition groupByCondition = new GroupByCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));

        GroupByOperatorNode groupByNode = new GroupByOperatorNode(
                groupByCondition,
                new TableNode("Admissions_5000", TableTypeEnum.FACT, "a", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category"));

        ProjectionCondition projectionCondition = new ProjectionCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

        MaterializedViewQueryTreeCollection.AddMaterializedViewToPrimary(new MaterializedViewQueryTree("MV1", projectionNode));
        /*-----------------------------*/

        groupByCondition = new GroupByCondition("sex", "admsourc", "admtype", "care", "sepmode", "age_category");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));

        groupByNode = new GroupByOperatorNode(
                groupByCondition,
                new TableNode("Admissions_5000", TableTypeEnum.FACT, "a", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category"));

        projectionCondition = new ProjectionCondition("sex", "admsourc", "admtype", "care", "sepmode", "age_category");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

        MaterializedViewQueryTreeCollection.AddMaterializedViewToPrimary(new MaterializedViewQueryTree("MV2", projectionNode));
        /*-----------------------------*/

        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(
                new TableNode("ADMISSIONS_5000", TableTypeEnum.FACT, "A", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category"),
                new TableNode("DIAGNOSIS_5000", TableTypeEnum.DIMENSION, "D", "admissions_pk", "position", "diag", "pref"));

        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(
                cartesianJoin1,
                new TableNode("DIAGNOSIS_DIM", TableTypeEnum.DIMENSION, "DM", "diagnosis_num", "diagnosis_desc", "code_range_start", "code_range_end"));

        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin2);

        groupByCondition = new GroupByCondition("sex");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));

        groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);

        projectionCondition = new ProjectionCondition("sex");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);

        MaterializedViewQueryTreeCollection.AddMaterializedViewToPrimary(new MaterializedViewQueryTree("MV3", projectionNode));
        /*-----------------------------*/
    }

    public void addContents(Stage stage) throws Exception {
        ObservableList<InputQueryTree> tableData = FXCollections.observableArrayList();

        TableColumn<InputQueryTree, String> sqlColumn
                = new TableColumn<InputQueryTree, String>("Sql");

        sqlColumn.setCellValueFactory(
                new PropertyValueFactory<InputQueryTree, String>("Sql"));

        TableView<InputQueryTree> tableView = new TableView<InputQueryTree>();

        tableView.getColumns().add(sqlColumn);

        tableView.setItems(tableData);

        //tableView.setMinWidth(600);
        //tableView.setMaxWidth(700);
        sqlColumn.setMinWidth(600);

        this.InitializeTableData(tableData);

        Button FindMVForQuery = new Button("Find MV For Query");
        FindMVForQuery.setOnAction((e) -> {
            InputQueryTree qt = tableView.getSelectionModel().getSelectedItem();
            if (qt != null) {
                try {
                    //System.out.println("Input query: \n" + qt.displayData());

                    long timeTaken = 0;
                    long start = System.nanoTime();
                    ArrayList<MaterializedViewQueryTree> result = MaterializedViewQueryTreeCollection.FindMatch(qt);
                    long end = System.nanoTime();

                    timeTaken += end - start;

                    if (result.isEmpty()) {
                        System.out.println("No match found");
                    } else {
                        System.out.println("Matching materialized view found");
                        for (MaterializedViewQueryTree materializedViewQueryTree : result) {

                            ResultSet rs = DBConnection.ExecuteQuery("select * from " + materializedViewQueryTree.name);
                            while (rs.next()) {
                                rs.toString();
                            }

                            // System.out.println(materializedViewQueryTree.displayData());
                        }

                        //timeTaken += end - start;

                        System.out.println("Query: " + qt.sql);
                        System.out.println("Total Time Taken with MV:" + timeTaken + "\n");
                    }
                } catch (Exception ex) {
                    Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        Button RunQueryDirectly = new Button("Run Query Directly");
        RunQueryDirectly.setOnAction((e) -> {
            InputQueryTree qt = tableView.getSelectionModel().getSelectedItem();
            if (qt != null) {
                try {
                    System.out.println("Input query: \n" + qt.displayData());

                    long timeTaken = 0;
                    long start = System.nanoTime();
                    ResultSet rs = DBConnection.ExecuteQuery(qt.getSql());
                    while (rs.next()) {
                        rs.toString();
                    }

                    long end = System.nanoTime();

                    timeTaken += end - start;

                    System.out.println("Query: " + qt.sql);
                    System.out.println("Total Time Taken for direct run:" + timeTaken + "\n");

                } catch (Exception ex) {
                    Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        Button executeAnalyser = new Button("Execute Analyser");
        executeAnalyser.setOnAction((e) -> {
            try {
                // new Analyser().start();
            } catch (Exception ex) {
                Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        Label l1 = new Label("Tables to track: ");
        TextField t1 = new TextField();

        Button submitTablesForTracking = new Button("Submit Tables");
        submitTablesForTracking.setOnAction((e) -> {
            try {
                // DW Change Tracker code
                DWChangeTracker.tableNames = t1.getText().split(",");
                DWChangeTracker.tables = DWChangeTracker.GetTableInformation();
                System.out.println("DW Tracker started");

                this.dwChangeTrackerTimer = new Timer();
                dwChangeTrackerTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            new DWChangeTracker().start();
                        } catch (Exception ex) {
                            Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }, DWChangeTracker.TIME_INTERVAL, DWChangeTracker.TIME_INTERVAL);                 
            } catch (Exception ex) {
                Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        HBox dwtrackerHBox = new HBox(l1, t1, submitTablesForTracking);
        dwtrackerHBox.setSpacing(10);

        HBox executeHBox = new HBox(FindMVForQuery, RunQueryDirectly, executeAnalyser);
        executeHBox.setAlignment(Pos.CENTER);
        executeHBox.setSpacing(10);

        HBox top = new HBox();
        HBox bottom = new HBox();
        top.minHeight(10);
        bottom.minHeight(10);

        VBox vbox = new VBox(top, dwtrackerHBox, tableView, executeHBox, bottom);

        vbox.setSpacing(10);
        vbox.setStyle("-fx-background-color: wheat; -fx-font-size: 20");
        vbox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vbox);
        stage.setScene(scene);
    }

    @Override
    public void stop() {
        if (this.analyserTimer != null) {
            this.analyserTimer.cancel();
            this.analyserTimer.purge();
            this.analyserTimer = null;
        }

        if (this.dwChangeTrackerTimer != null) {
            this.dwChangeTrackerTimer.cancel();
            this.dwChangeTrackerTimer.purge();
            this.dwChangeTrackerTimer = null;
        }
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
