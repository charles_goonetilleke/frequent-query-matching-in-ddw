package DomainMatchingExperiments;

import querytree.InputQueryTree;
import querytree.QueryMatching;
import querytree.MaterializedViewQueryTree;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import querytree.enums.StatusEnum;

public final class MaterializedViewQueryTreeCollection {

    public static HashMap<String, ArrayList<MaterializedViewQueryTree>> BPTree_Key_TableName;
    public static HashMap<Integer, MaterializedViewQueryTree> BPTree_Key_MVName;
    public static ArrayList<MaterializedViewQueryTree> ArrayList_MVs;

    public static void Init() throws SQLException {
        // B+ tree with table names as keys
        // The value of each key is a list of query trees that have that tree
        BPTree_Key_TableName = new HashMap<String, ArrayList<MaterializedViewQueryTree>>();

        // B+ tree with query tree id as key where each query tree 
        // The value is the query tree
        BPTree_Key_MVName = new HashMap<Integer, MaterializedViewQueryTree>();

        // Plain list of mvs
        ArrayList_MVs = new ArrayList<MaterializedViewQueryTree>();
    }

    public static ArrayList<MaterializedViewQueryTree> Search_BPTree_Key_TableName(InputQueryTree input) throws Exception {
        ArrayList<MaterializedViewQueryTree> result = new ArrayList<MaterializedViewQueryTree>();
        ArrayList<MaterializedViewQueryTree> materializedViewQueryTrees = new ArrayList<MaterializedViewQueryTree>();
        ArrayList<String> tableNames = input.root.GetTableNames();
        HashMap<String, ArrayList<MaterializedViewQueryTree>> trees = new HashMap<String, ArrayList<MaterializedViewQueryTree>>();
        HashMap<MaterializedViewQueryTree, Integer> counts = new HashMap<MaterializedViewQueryTree, Integer>();

        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> value = BPTree_Key_TableName.get(table);
            if (value != null) {
                trees.put(table, value);
            } else {
                trees.put(table, new ArrayList<MaterializedViewQueryTree>());
            }
        }

        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> temp = trees.get(table);
            for (MaterializedViewQueryTree q : temp) {
                Integer i = counts.get(q);
                if (i == null) {
                    counts.put(q, 1);
                } else {
                    counts.replace(q, ++i);
                }
            }
        }

        Set<MaterializedViewQueryTree> keyset = counts.keySet();
        for (MaterializedViewQueryTree q : keyset) {
            Integer i = counts.get(q);
            if (i == tableNames.size()) {
                materializedViewQueryTrees.add(q);
            }
        }

        for (MaterializedViewQueryTree mv : materializedViewQueryTrees) {
            double score = new QueryMatching(mv, input).Calculate();

            if (score == 4) {
                mv.currentSessionHits++;
                result.add(mv);
            }
        }

        return result;
    }

    public static ArrayList<MaterializedViewQueryTree> Search_BPTree_Key_MVName(InputQueryTree input) throws Exception {
        ArrayList<MaterializedViewQueryTree> result = new ArrayList<MaterializedViewQueryTree>();
        ArrayList<MaterializedViewQueryTree> materializedViewQueryTrees = new ArrayList<MaterializedViewQueryTree>();
        ArrayList<String> tableNames = input.root.GetTableNames();

        BPTree_Key_MVName.forEach((k, v) -> {
            try {
                ArrayList<String> temp = v.root.GetTableNames();
                boolean tableMatch = true;

                if (temp.size() != tableNames.size()) {
                    tableMatch = false;
                } else {
                    for (String s : temp) {
                        if (!tableNames.contains(s)) {
                            tableMatch = false;
                        }
                    }
                }

                if (tableMatch) {
                    materializedViewQueryTrees.add(v);
                }
            } catch (Exception ex) {
                Logger.getLogger(MaterializedViewQueryTreeCollection.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        for (MaterializedViewQueryTree mv : materializedViewQueryTrees) {
            double score = new QueryMatching(mv, input).Calculate();

            if (score == 4) {
                mv.currentSessionHits++;
                result.add(mv);
            }
        }

        return result;
    }

    public static ArrayList<MaterializedViewQueryTree> Search_ArrayList_MVs(InputQueryTree input) throws Exception {
        ArrayList<MaterializedViewQueryTree> result = new ArrayList<MaterializedViewQueryTree>();
        ArrayList<String> tableNames = input.root.GetTableNames();

        ArrayList_MVs.forEach((e) -> {
            try {
                ArrayList<String> temp = e.root.GetTableNames();
                boolean tableMatch = true;

                if (temp.size() != tableNames.size()) {
                    tableMatch = false;
                } else {
                    for (String s : temp) {
                        if (!tableNames.contains(s)) {
                            tableMatch = false;
                        }
                    }
                }

                if (tableMatch) {
                    double score = new QueryMatching(e, input).Calculate();

                    if (score == 4) {
                        e.currentSessionHits++;
                        result.add(e);
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(MaterializedViewQueryTreeCollection.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        return result;
    }

    public static void AddMaterializedViewToCollections(MaterializedViewQueryTree tree) throws Exception {
        tree.status = StatusEnum.ACTIVE;
        ArrayList<String> tableNames = tree.root.GetTableNames();
        for (String table : tableNames) {
            ArrayList<MaterializedViewQueryTree> value = BPTree_Key_TableName.get(table);
            if (value != null) {
                if (!value.contains(tree)) {
                    value.add(tree);
                }
            } else {
                value = new ArrayList<>();
                value.add(tree);
                BPTree_Key_TableName.put(table, value);
            }
        }

        BPTree_Key_MVName.putIfAbsent(tree.id, tree);

        ArrayList_MVs.add(tree);
    }
}
