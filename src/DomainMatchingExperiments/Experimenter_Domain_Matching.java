package DomainMatchingExperiments;

import querytree.structure.conditions.AggregateFunction;
import querytree.structure.conditions.Statement;
import querytree.structure.conditions.ProjectionCondition;
import querytree.structure.conditions.SelectionCondition;
import querytree.structure.conditions.GroupByCondition;
import querytree.structure.SelectionOperatorNode;
import querytree.structure.ProjectionOperatorNode;
import querytree.structure.GroupByOperatorNode;
import querytree.structure.TableNode;
import querytree.structure.CartesianProductOperatorNode;
import querytree.enums.AggregateFunctionEnum;
import querytree.enums.TableTypeEnum;
import querytree.enums.RelationalOperatorEnum;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import querytree.InputQueryTree;
import querytree.MaterializedViewQueryTree;

public class Experimenter_Domain_Matching extends Application {

    private static String Admissions = "";
    private static String Diagnosis = "";

    private static final TableNode DIAGNOSIS_DIM_NODE = new TableNode("DIAGNOSIS_DIM", TableTypeEnum.DIMENSION, "DM", "diagnosis_num", "diagnosis_desc", "code_range_start", "code_range_end");
    private static final TableNode ADMSOURCE_DIM_NODE = new TableNode("ADMSOURCE_DIM", TableTypeEnum.DIMENSION, "ADS", "vaed_value", "cd_desc");
    private static final TableNode ADMTYPE_DIM_NODE = new TableNode("ADMTYPE_DIM", TableTypeEnum.DIMENSION, "ATP", "vaed_value", "cd_desc");
    private static final TableNode CARE_TYPE_DIM_NODE = new TableNode("CARE_TYPE_DIM", TableTypeEnum.DIMENSION, "C", "vaed_value", "cd_desc");
    private static final TableNode SEPARATION_MODE_DIM_NODE = new TableNode("SEPARATION_MODE_DIM", TableTypeEnum.DIMENSION, "S", "vaed_value", "cd_desc");
    private static final TableNode ADMTIME_DIM_NODE = new TableNode("ADMTIME_DIM", TableTypeEnum.DIMENSION, "ADT", "admtimeid", "admyear", "admmonth");
    private static final TableNode AGE_DIM_NODE = new TableNode("AGE_DIM", TableTypeEnum.DIMENSION, "AG", "age_category", "age_desc", "start_age", "end_age");
    private static final TableNode GENDER_DIM_NODE = new TableNode("GENDER_DIM", TableTypeEnum.DIMENSION, "G", "gender_number", "gender_desc");

    @Override
    public void start(Stage stage) throws Exception {
        addContents(stage);
        stage.setResizable(true);
        stage.setTitle(getClass().getName());
        stage.show();
    }

    public static ProjectionOperatorNode ManyJoins(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode ManyGroupBys(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, ADMTIME_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin1);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode ManySelections(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYY')", "'2007'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'mm')", "'12'", RelationalOperatorEnum.EQUAL_TO));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, ADMISSIONS_NODE);
        GroupByCondition groupByCondition = new GroupByCondition("A.AGE_CATEGORY");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.AGE_CATEGORY");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode ManyJoinsSelectionsGroupBys(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, ADMSOURCE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin3 = new CartesianProductOperatorNode(cartesianJoin2, ADMTIME_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin4 = new CartesianProductOperatorNode(cartesianJoin3, ADMTYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin5 = new CartesianProductOperatorNode(cartesianJoin4, AGE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin6 = new CartesianProductOperatorNode(cartesianJoin5, GENDER_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin7 = new CartesianProductOperatorNode(cartesianJoin6, CARE_TYPE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin8 = new CartesianProductOperatorNode(cartesianJoin7, SEPARATION_MODE_DIM_NODE);
        CartesianProductOperatorNode cartesianJoin9 = new CartesianProductOperatorNode(cartesianJoin8, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEX", "G.GENDER_NUMBER", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "ADS.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "ATP.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "C.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "S.VAED_VALUE", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.AGE_CATEGORY", "AG.AGE_CATEGORY", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMSOURC", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.ADMTYPE", "'X'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.CARE", "'4'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("A.SEPMODE", "'H'", RelationalOperatorEnum.EQUAL_TO),
                new Statement("TO_CHAR(A.ADMDATE, 'YYYYMM')", "ADT.ADMTIMEID", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin9);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX", "A.ADMSOURC", "A.ADMTYPE", "A.CARE", "A.SEPMODE", "A.AGE_CATEGORY", "ADT.ADMYEAR", "ADT.ADMMONTH");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode SpecialFunctionInSelection(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        CartesianProductOperatorNode cartesianJoin1 = new CartesianProductOperatorNode(ADMISSIONS_NODE, DIAGNOSIS_NODE);
        CartesianProductOperatorNode cartesianJoin2 = new CartesianProductOperatorNode(cartesianJoin1, DIAGNOSIS_DIM_NODE);
        SelectionCondition selectionCondition = new SelectionCondition(
                new Statement("A.PK", "D.ADMISSIONS_PK", RelationalOperatorEnum.EQUAL_TO),
                new Statement("SUBSTR(D.DIAG, 1, 1)", "SUBSTR(DM.CODE_RANGE_START, 1, 1)", RelationalOperatorEnum.EQUAL_TO),
                new Statement("DM.DIAGNOSIS_DESC", "'Diseases%'", RelationalOperatorEnum.LIKE));
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, cartesianJoin2);
        GroupByCondition groupByCondition = new GroupByCondition("A.SEX");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("A.SEX");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public static ProjectionOperatorNode To_CharInGroupBy(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        SelectionCondition selectionCondition = new SelectionCondition();
        SelectionOperatorNode selectionNode = new SelectionOperatorNode(selectionCondition, ADMISSIONS_NODE);
        GroupByCondition groupByCondition = new GroupByCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
        groupByCondition.aggregateFunctions.add(new AggregateFunction("SUM(1)", AggregateFunctionEnum.SUM, "SUM(1)"));
        GroupByOperatorNode groupByNode = new GroupByOperatorNode(groupByCondition, selectionNode);
        ProjectionCondition projectionCondition = new ProjectionCondition("TO_CHAR(ADMDATE, 'YYYY')", "TO_CHAR(ADMDATE, 'MM')");
        projectionCondition.aggregateFunctions = groupByCondition.aggregateFunctions;
        ProjectionOperatorNode projectionNode = new ProjectionOperatorNode(projectionCondition, groupByNode);
        return projectionNode;
    }

    public void CollectionDetails() {
        /*StringWriter out = new StringWriter();
        MaterializedViewQueryTreeCollection.BPTree_Key_MVName.printXml(out);
        System.out.println("For MVName:\n" + out);
        
        out = new StringWriter();
        MaterializedViewQueryTreeCollection.BPTree_Key_TableName.printXml(out);
        System.out.println("For TableName:\n" + out);*/

        System.out.println("BPTree_Key_TableName size: " + MaterializedViewQueryTreeCollection.BPTree_Key_TableName.size());
        System.out.println("BPTree_Key_MVName size: " + MaterializedViewQueryTreeCollection.BPTree_Key_MVName.size());
        System.out.println("ArrayList_MVs size: " + MaterializedViewQueryTreeCollection.ArrayList_MVs.size());
    }

    public void AddMVsToCollection(TableNode ADMISSIONS_NODE, TableNode DIAGNOSIS_NODE) throws Exception {
        MaterializedViewQueryTreeCollection.AddMaterializedViewToCollections(new MaterializedViewQueryTree(ManyJoins(ADMISSIONS_NODE, DIAGNOSIS_NODE)));
        MaterializedViewQueryTreeCollection.AddMaterializedViewToCollections(new MaterializedViewQueryTree(ManyGroupBys(ADMISSIONS_NODE, DIAGNOSIS_NODE)));
        MaterializedViewQueryTreeCollection.AddMaterializedViewToCollections(new MaterializedViewQueryTree(ManySelections(ADMISSIONS_NODE, DIAGNOSIS_NODE)));
        MaterializedViewQueryTreeCollection.AddMaterializedViewToCollections(new MaterializedViewQueryTree(ManyJoinsSelectionsGroupBys(ADMISSIONS_NODE, DIAGNOSIS_NODE)));
        MaterializedViewQueryTreeCollection.AddMaterializedViewToCollections(new MaterializedViewQueryTree(SpecialFunctionInSelection(ADMISSIONS_NODE, DIAGNOSIS_NODE)));
        //MaterializedViewQueryTreeCollection.AddMaterializedViewToCollections(new MaterializedViewQueryTree(To_CharInGroupBy(ADMISSIONS_NODE, DIAGNOSIS_NODE)));
    }

    public void addContents(Stage stage) throws Exception {
        Button Calculate = new Button("Calculate");
        Calculate.setOnAction((e) -> {
            try (PrintWriter printToFile = new PrintWriter(new File("DomainMatchingResults.txt"))) {
                //PrintWriter printToFile2 = new PrintWriter(new File("DomainMatchingResults_collection_sizes.txt"));
                ArrayList<String> partitions = new ArrayList<String>();

                TableNode ADMISSIONS_NODE = new TableNode("Admissions", TableTypeEnum.FACT, "A", "pk", "sex", "admsourc", "admtype", "care", "sepmode", "admdate", "age_category");
                TableNode DIAGNOSIS_NODE = new TableNode("Diagnosis", TableTypeEnum.DIMENSION, "D", "admissions_pk", "position", "diag", "pref");
                InputQueryTree qt = new InputQueryTree(ManyJoinsSelectionsGroupBys(ADMISSIONS_NODE, DIAGNOSIS_NODE));

                MaterializedViewQueryTreeCollection.Init();

                partitions.add("");

                partitions.add("_100");
                partitions.add("_200");
                partitions.add("_300");
                partitions.add("_400");
                partitions.add("_500");

                partitions.add("_600");
                partitions.add("_700");
                partitions.add("_800");
                partitions.add("_900");
                partitions.add("_1000");

                partitions.add("_1100");
                partitions.add("_12000");
                partitions.add("_1300");
                partitions.add("_1400");
                partitions.add("_1500");

                partitions.add("_1600");
                partitions.add("_1700");
                partitions.add("_1800");
                partitions.add("_1900");
                partitions.add("_2000");
                //
                partitions.add("_3000");
                partitions.add("_4000");
                partitions.add("_5000");

                partitions.add("_6000");
                partitions.add("_7000");
                partitions.add("_8000");
                partitions.add("_9000");
                partitions.add("_10000");

                partitions.add("_11000");
                partitions.add("_120000");
                partitions.add("_13000");
                partitions.add("_14000");
                partitions.add("_15000");

                partitions.add("_16000");
                partitions.add("_17000");
                partitions.add("_18000");
                partitions.add("_19000");
                partitions.add("_20000");
                //
                partitions.add("_30000");
                partitions.add("_40000");
                partitions.add("_50000");

                partitions.add("_60000");
                partitions.add("_70000");
                partitions.add("_80000");
                partitions.add("_90000");
                partitions.add("_100000");

                partitions.add("_110000");
                partitions.add("_1200000");
                partitions.add("_130000");
                partitions.add("_140000");
                partitions.add("_150000");

                partitions.add("_160000");
                partitions.add("_170000");
                partitions.add("_180000");
                partitions.add("_190000");
                partitions.add("_200000");
                //
                partitions.add("_300000");
                partitions.add("_400000");
                partitions.add("_500000");

                partitions.add("_600000");
                partitions.add("_700000");
                partitions.add("_800000");
                partitions.add("_900000");
                partitions.add("_1000000");

                partitions.add("_1100000");
                partitions.add("_1200000");
                partitions.add("_1300000");
                partitions.add("_1400000");
                partitions.add("_1500000");

                System.out.println(partitions.size());
                for (String partition : partitions) {
                    double totaltime = 0;
                    ADMISSIONS_NODE.tableName = "Admissions" + partition;
                    DIAGNOSIS_NODE.tableName = "Diagnosis" + partition;

                    AddMVsToCollection(ADMISSIONS_NODE, DIAGNOSIS_NODE);
                    CollectionDetails();

                    for (int i = 0; i < 50; ++i) {
                        long timeTaken = 0;

                        // Time taken to perform MV-to-qery matching
                        long start = System.nanoTime();
                        ArrayList<MaterializedViewQueryTree> result = MaterializedViewQueryTreeCollection.Search_BPTree_Key_TableName(qt);
                        long end = System.nanoTime();

                        timeTaken += (end - start);

                        if (result.isEmpty()) {
                            if (i == 0) {
                                System.out.println("No match found");
                            }
                        } else {
                            if (i == 0) {
                                System.out.println("Matching materialized view found: " + result.get(0));
                            }

                            // Only sum up the last 35
                            if (i >= 15) {
                                totaltime += timeTaken;
                            }
                        }
                    }

                    totaltime = totaltime / 35;
                    System.out.println("Number of items in BPTree_Key_TableName: " + MaterializedViewQueryTreeCollection.BPTree_Key_TableName.size());
                    System.out.println("Total Time Taken to find match using BPTree_Key_TableName:" + totaltime + "\n");
                    printToFile.print(MaterializedViewQueryTreeCollection.ArrayList_MVs.size() + "\t");
                    //printToFile2.print(MaterializedViewQueryTreeCollection.BPTree_Key_TableName.size() + "\t");
                    printToFile.print(totaltime + "\t");

                    totaltime = 0;
                    for (int i = 0; i < 50; ++i) {
                        long timeTaken = 0;

                        // Time taken to perform MV-to-qery matching
                        long start = System.nanoTime();
                        ArrayList<MaterializedViewQueryTree> result = MaterializedViewQueryTreeCollection.Search_BPTree_Key_MVName(qt);
                        long end = System.nanoTime();

                        timeTaken += (end - start);

                        if (result.isEmpty()) {
                            if (i == 0) {
                                System.out.println("No match found");
                            }
                        } else {
                            if (i == 0) {
                                System.out.println("Matching materialized view found: " + result.get(0));
                            }

                            // Only sum up the last 35
                            if (i >= 15) {
                                totaltime += timeTaken;
                            }
                        }
                    }

                    totaltime = totaltime / 35;
                    System.out.println("Number of items in BPTree_Key_MVName: " + MaterializedViewQueryTreeCollection.BPTree_Key_MVName.size());
                    System.out.println("Total Time Taken to find match BPTree_Key_MVName:" + totaltime + "\n");
                    //printToFile2.print(MaterializedViewQueryTreeCollection.BPTree_Key_MVName.size() + "\t");
                    printToFile.print(totaltime + "\t");

                    totaltime = 0;
                    for (int i = 0; i < 50; ++i) {
                        long timeTaken = 0;

                        // Time taken to perform MV-to-qery matching
                        long start = System.nanoTime();
                        ArrayList<MaterializedViewQueryTree> result = MaterializedViewQueryTreeCollection.Search_ArrayList_MVs(qt);
                        long end = System.nanoTime();

                        timeTaken += (end - start);

                        if (result.isEmpty()) {
                            if (i == 0) {
                                System.out.println("No match found");
                            }
                        } else {
                            if (i == 0) {
                                System.out.println("Matching materialized view found: " + result.get(0));
                            }

                            // Only sum up the last 35
                            if (i >= 15) {
                                totaltime += timeTaken;
                            }
                        }
                    }

                    totaltime = totaltime / 35;
                    System.out.println("Number of items in ArrayList_MVs: " + MaterializedViewQueryTreeCollection.ArrayList_MVs.size());
                    System.out.println("Total Time Taken to find match ArrayList_MVs:" + totaltime + "\n");
                    //printToFile.print( MaterializedViewQueryTreeCollection.ArrayList_MVs.size() + "\n");
                    printToFile.print(totaltime + "\n");
                }

                printToFile.flush();
                printToFile.close();

            } catch (Exception ex) {
                Logger.getLogger(Experimenter_Domain_Matching.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.out.println("Calculations are complete");
        });

        HBox executeHBox = new HBox(Calculate);
        executeHBox.setAlignment(Pos.CENTER);
        executeHBox.setSpacing(10);

        HBox top = new HBox();
        HBox bottom = new HBox();
        top.minHeight(10);
        bottom.minHeight(10);

        VBox vbox = new VBox(top, executeHBox, bottom);

        vbox.setSpacing(10);
        vbox.setStyle("-fx-background-color: wheat; -fx-font-size: 20");
        vbox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vbox, 300, 200);
        stage.setScene(scene);
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
