package samplequeries;

import java.util.HashMap;
import querytree.enums.AggregateFunctionEnum;
import querytree.enums.RelationalOperatorEnum;
import querytree.enums.TableTypeEnum;
import querytree.structure.GroupByOperatorNode;
import querytree.structure.OperatorNode;
import querytree.structure.ProjectionOperatorNode;
import querytree.structure.TableNode;
import querytree.structure.conditions.AggregateFunction;
import querytree.structure.conditions.GroupByCondition;
import querytree.structure.conditions.ProjectionCondition;

public class LabUsageQueries {
    /*
    public static void main(String[] args) throws Exception{
        
        QueryA();/*
        QueryB1();
        QueryB2();
        QueryC();
        
        
        //QueryTreeSimilarity s = new QueryTreeMatching(QueryA(), QueryC());
        //s.PrintStats();
    }

    public static OperatorNode QueryA() throws Exception {
        //select u.timeid, l.time_desc, sum(u.total_usage)
        //from lab_usage_usefact u, lab_usage_labtime l
        //where u.timeid = l.timeid
        //group by u.timeid, l.time_desc;        
        
        JoinOperatorNode join1 = new JoinOperatorNode(new JoinCondition("u.timeid", "l.timeid", RelationalOperatorEnum.EQUAL_TO));
        join1.AddChildren(GetStarSchemaTables().get("lab_usage_usefact"), GetStarSchemaTables().get("lab_usage_labtime"));
                       
        GroupByOperatorNode groupBy = new GroupByOperatorNode(new GroupByCondition("u.time_id", "l.time_desc"));
        ((GroupByCondition)groupBy.condition).aggregateFunctions.add(new AggregateFunction("s", AggregateFunctionEnum.SUM, "u.total_usage"));
        groupBy.AddChildren(join1);
        
        ProjectionOperatorNode projection = new ProjectionOperatorNode(new ProjectionCondition("u.timeid", "l.time_desc"));
        projection.AddChildren(groupBy);
        
        System.out.println(projection.GetTableNames());
        QueryTreePrinter.printNode(projection);
        System.out.println("");
        
        return projection;
    }

    public static OperatorNode QueryB1() throws Exception {
        //select u.timeid, u.major_code, u.class_id, sum(u.total_usage)
        //from lab_usage_usefact u
        //group by u.timeid, u.major_code, u.class_id;
        
        GroupByOperatorNode groupBy = new GroupByOperatorNode(new GroupByCondition("u.time_id", "u.major_code", "u.class_id"));
        ((GroupByCondition)groupBy.condition).aggregateFunctions.add(new AggregateFunction("s", AggregateFunctionEnum.SUM, "u.total_usage"));
        groupBy.AddChildren(GetStarSchemaTables().get("lab_usage_usefact"));
        
        ProjectionOperatorNode projection = new ProjectionOperatorNode(new ProjectionCondition("u.timeid", "u.major_code", "u.class_id"));
        projection.AddChildren(groupBy);
        
        System.out.println(projection.GetTableNames());
        QueryTreePrinter.printNode(projection);
        System.out.println("");
        
        return projection;
    }

    public static OperatorNode QueryB2() throws Exception {
        //select t.timeid, t.time_desc, m.major_code, m.major_name, c.class_id, c.class_description,
        //sum(u.total_usage)
        //from lab_usage_usefact u, lab_usage_majordim m, lab_usage_classdim c, lab_usage_labtime t
        //where u.timeid = t.timeid
        //and u.major_code = m.major_code
        //and u.class_id = c.class_id
        //group by t.timeid, t.time_desc, m.major_code, m.major_name, c.class_id, c.class_description;
        
        JoinOperatorNode join1 = new JoinOperatorNode(new JoinCondition("u.timeid", "l.timeid", RelationalOperatorEnum.EQUAL_TO));
        join1.AddChildren(GetStarSchemaTables().get("lab_usage_usefact"), GetStarSchemaTables().get("lab_usage_labtime"));
        
        JoinOperatorNode join2 = new JoinOperatorNode(new JoinCondition("u.major_code", "m.major_code", RelationalOperatorEnum.EQUAL_TO));
        join2.AddChildren(join1, GetStarSchemaTables().get("lab_usage_majordim"));
        
        JoinOperatorNode join3 = new JoinOperatorNode(new JoinCondition("u.class_id", "c.class_id", RelationalOperatorEnum.EQUAL_TO));
        join3.AddChildren(join2, GetStarSchemaTables().get("lab_usage_classdim"));
        
        GroupByOperatorNode groupBy = new GroupByOperatorNode(new GroupByCondition("l.timeid", "l.time_desc", "m.major_code", "m.major_name", "c.class_id", "c.class_description"));
        ((GroupByCondition)groupBy.condition).aggregateFunctions.add(new AggregateFunction("s", AggregateFunctionEnum.SUM, "u.total_usage"));
        groupBy.AddChildren(join3);
        
        ProjectionOperatorNode projection = new ProjectionOperatorNode(new ProjectionCondition("l.timeid", "l.time_desc", "m.major_code", "m.major_name", "c.class_id", "c.class_description"));
        projection.AddChildren(groupBy);
        
        System.out.println(projection.GetTableNames());
        QueryTreePrinter.printNode(projection);
        System.out.println("");
        
        return projection;
    }
    
    public static OperatorNode QueryC() throws Exception{
        //select u.major_code, u.semid, sum(u.total_usage)
        //from lab_usage_usefact u
        //group by u.major_code, u.semid;       
        
        GroupByOperatorNode groupBy = new GroupByOperatorNode(new GroupByCondition("u.major_code", "u.semid"));
        ((GroupByCondition)groupBy.condition).aggregateFunctions.add(new AggregateFunction("s", AggregateFunctionEnum.SUM, "u.total_usage"));
        groupBy.AddChildren(GetStarSchemaTables().get("lab_usage_usefact"));
        
        ProjectionOperatorNode projection = new ProjectionOperatorNode(new ProjectionCondition("u.major_code", "u.semid"));
        projection.AddChildren(groupBy);
        
        System.out.println(projection.GetTableNames());
        QueryTreePrinter.printNode(projection);
        System.out.println("");
        
        return projection;
    }
    
    public static HashMap<String, TableNode> GetStarSchemaTables(){
        HashMap<String, TableNode> tables = new HashMap<String, TableNode>();
                
        TableNode lab_usage_majordim = new TableNode("lab_usage_majordim", TableTypeEnum.DIMENSION, "m", "major_name", "major_code");
        TableNode lab_usage_classdim = new TableNode("lab_usage_classdim", TableTypeEnum.DIMENSION, "c", "class_description", "class_id");
        TableNode lab_usage_labtime = new TableNode("lab_usage_labtime", TableTypeEnum.DIMENSION, "l", "timeid", "time_desc", "begin_time", "end_time");
        TableNode lab_usage_db_semester = new TableNode("lab_usage_db_semester", TableTypeEnum.DIMENSION, "s", "semid", "sem_desc", "begin_time", "end_time");
        TableNode lab_usage_usefact = new TableNode("lab_usage_usefact", TableTypeEnum.FACT, "u", "semid", "timeid", "class_id", "major_code", "total_usage");        
        
        tables.put(lab_usage_majordim.tableName, lab_usage_majordim);
        tables.put(lab_usage_classdim.tableName, lab_usage_classdim);
        tables.put(lab_usage_labtime.tableName, lab_usage_labtime);
        tables.put(lab_usage_db_semester.tableName, lab_usage_db_semester);
        tables.put(lab_usage_usefact.tableName, lab_usage_usefact);
 
        return tables;
    }
*/
}
