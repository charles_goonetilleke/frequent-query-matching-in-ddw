package samplequeries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import querytree.structure.BaseNode;

class QueryTreePrinter {

    public static <T extends Comparable<?>> void printNode(BaseNode root) {
        int maxLevel = QueryTreePrinter.maxLevel(root);

        printNodeInternal(Collections.singletonList(root), 1, maxLevel);
    }

    private static <T extends Comparable<?>> void printNodeInternal(List<BaseNode> nodes, int level, int maxLevel) {
        if (nodes.isEmpty() || QueryTreePrinter.isAllElementsNull(nodes)) {
            return;
        }

        int floor = maxLevel - level;
        int endgeLines = (int) Math.pow(2, (Math.max(floor - 1, 0)));
        int firstSpaces = (int) Math.pow(2, (floor)) - 1;
        int betweenSpaces = (int) Math.pow(2, (floor + 1)) - 1;

        QueryTreePrinter.printWhitespaces(firstSpaces);

        List<BaseNode> newNodes = new ArrayList<BaseNode>();
        for (BaseNode node : nodes) {
            if (node != null && node.children != null) {
                System.out.print(node.toString());

                if (node.children.size() == 1) {
                    newNodes.add(node.children.get(0));
                    newNodes.add(null);
                } else {
                    newNodes.add(node.children.get(0));
                    newNodes.add(node.children.get(1));
                }
            } 
            else if(node != null && node.children == null){
                System.out.print(node.toString());
                newNodes.add(null);
                newNodes.add(null);
            }
            else {
                newNodes.add(null);
                newNodes.add(null);
                System.out.print(" ");
            }

            QueryTreePrinter.printWhitespaces(betweenSpaces);
        }
        System.out.println("");

        for (int i = 1; i <= endgeLines; i++) {
            for (int j = 0; j < nodes.size(); j++) {
                QueryTreePrinter.printWhitespaces(firstSpaces - i);
                if (nodes.get(j) == null) {
                    QueryTreePrinter.printWhitespaces(endgeLines + endgeLines + i + 1);
                    continue;
                }

                if (nodes.get(j).children != null && nodes.get(j).children.get(0) != null) {
                    System.out.print("/");
                } else {
                    QueryTreePrinter.printWhitespaces(1);
                }

                QueryTreePrinter.printWhitespaces(i + i - 1);

                if (nodes.get(j).children != null && nodes.get(j).children.get(1) != null) {
                    System.out.print("\\");
                } else {
                    QueryTreePrinter.printWhitespaces(1);
                }

                QueryTreePrinter.printWhitespaces(endgeLines + endgeLines - i);
            }

            System.out.println("");
        }

        printNodeInternal(newNodes, level + 1, maxLevel);
    }

    private static void printWhitespaces(int count) {
        for (int i = 0; i < count; i++) {
            System.out.print(" ");
        }
    }

    private static <T extends Comparable<?>> int maxLevel(BaseNode node) {
        if (node == null || node.children == null) {
            return 0;
        }

        if (node.children.size() == 1) {
            return Math.max(QueryTreePrinter.maxLevel(node.children.get(0)), QueryTreePrinter.maxLevel(null)) + 1;
        } else {
            return Math.max(QueryTreePrinter.maxLevel(node.children.get(0)), QueryTreePrinter.maxLevel(node.children.get(1))) + 1;
        }
    }

    private static <T> boolean isAllElementsNull(List<T> list) {
        for (Object object : list) {
            if (object != null) {
                return false;
            }
        }

        return true;
    }

}
