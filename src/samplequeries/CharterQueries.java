package samplequeries;

import java.util.*;

public class CharterQueries {
/*
    public static void main(String[] args) throws Exception {
        AllGroupingAttributes();
    }

    public static ArrayList<OperatorNode> AllGroupingAttributes() throws Exception {
        ArrayList<OperatorNode> result = new ArrayList<OperatorNode>();
        TableNode c = GetStarSchemaTables().get("charter_fact");

        // SELECT TIME_ID, MOD_CODE, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE, EMP_NUM;
        GroupByCondition con = new GroupByCondition("time_id", "mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        GroupByOperatorNode g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        ProjectionOperatorNode p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);
        //
        
        // SELECT TIME_ID, MOD_CODE, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE, EMP_NUM;
        con = new GroupByCondition("time_id", "mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);
        //
        
        // SELECT TIME_ID, MOD_CODE, EMP_NUM, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE, EMP_NUM;
        con = new GroupByCondition("time_id", "mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);
        //
        
        // SELECT TIME_ID, MOD_CODE, EMP_NUM, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE, EMP_NUM;
        con = new GroupByCondition("time_id", "mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);
        //
        
        // SELECT TIME_ID, MOD_CODE, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE, EMP_NUM;
        con = new GroupByCondition("time_id", "mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);
        //
        
        // SELECT TIME_ID, MOD_CODE, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE, EMP_NUM;
        con = new GroupByCondition("time_id", "mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);
        //
        
        // SELECT TIME_ID, MOD_CODE, EMP_NUM, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE, EMP_NUM;
        con = new GroupByCondition("time_id", "mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);
        //

        return result;
    }

    public static ArrayList<OperatorNode> TwoGroupingAttributes_time_id_and_mode_code() throws Exception {
        ArrayList<OperatorNode> result = new ArrayList<OperatorNode>();
        TableNode c = GetStarSchemaTables().get("charter_fact");

        // SELECT TIME_ID, MOD_CODE, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE;
        GroupByCondition con = new GroupByCondition("time_id", "mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        GroupByOperatorNode g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        ProjectionOperatorNode p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, MOD_CODE, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE;
        con = new GroupByCondition("time_id", "mod_code");
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, MOD_CODE, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE;
        con = new GroupByCondition("time_id", "mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, MOD_CODE, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE;
        con = new GroupByCondition("time_id", "mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, MOD_CODE, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE;
        con = new GroupByCondition("time_id", "mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, MOD_CODE, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE;
        con = new GroupByCondition("time_id", "mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, MOD_CODE, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, MOD_CODE;
        con = new GroupByCondition("time_id", "mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        return result;
    }
    
    public static ArrayList<OperatorNode> TwoGroupingAttributes_time_id_and_emp_num() throws Exception {
        ArrayList<OperatorNode> result = new ArrayList<OperatorNode>();
        TableNode c = GetStarSchemaTables().get("charter_fact");

        // SELECT TIME_ID, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, EMP_NUM;
        GroupByCondition con = new GroupByCondition("time_id", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        GroupByOperatorNode g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        ProjectionOperatorNode p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, EMP_NUM, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, EMP_NUM;
        con = new GroupByCondition("time_id", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, EMP_NUM;
        con = new GroupByCondition("time_id", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, EMP_NUM, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, EMP_NUM;
        con = new GroupByCondition("time_id", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, EMP_NUM;
        con = new GroupByCondition("time_id", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, EMP_NUM, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, EMP_NUM;
        con = new GroupByCondition("time_id", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT TIME_ID, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID, EMP_NUM;
        con = new GroupByCondition("time_id", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        return result;
    }
    
    public static ArrayList<OperatorNode> TwoGroupingAttributes_mod_code_and_emp_num() throws Exception {
        ArrayList<OperatorNode> result = new ArrayList<OperatorNode>();
        TableNode c = GetStarSchemaTables().get("charter_fact");

        // SELECT MOD_CODE, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE, EMP_NUM;
        GroupByCondition con = new GroupByCondition("mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        GroupByOperatorNode g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        ProjectionOperatorNode p = new ProjectionOperatorNode(new ProjectionCondition("mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT MOD_CODE, EMP_NUM, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE, EMP_NUM;
        con = new GroupByCondition("mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT MOD_CODE, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE, EMP_NUM;
        con = new GroupByCondition("mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT MOD_CODE, EMP_NUM, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE, EMP_NUM;
        con = new GroupByCondition("mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT MOD_CODE, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE, EMP_NUM;
        con = new GroupByCondition("mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT MOD_CODE, EMP_NUM, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE, EMP_NUM;
        con = new GroupByCondition("mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        // SELECT MOD_CODE, EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE, EMP_NUM;
        con = new GroupByCondition("mod_code", "emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code", "emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //

        return result;
    }
    
    public static ArrayList<OperatorNode> OneGroupingAttribute_mod_code() throws Exception {
        ArrayList<OperatorNode> result = new ArrayList<OperatorNode>();
        TableNode c = GetStarSchemaTables().get("charter_fact");
        
        // SELECT MOD_CODE, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE;
        GroupByCondition con = new GroupByCondition("mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        GroupByOperatorNode g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        ProjectionOperatorNode p = new ProjectionOperatorNode(new ProjectionCondition("mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT MOD_CODE, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE;
        con = new GroupByCondition("mod_code");
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT MOD_CODE, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE;
        con = new GroupByCondition("mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT MOD_CODE, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE;
        con = new GroupByCondition("mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT MOD_CODE, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE;
        con = new GroupByCondition("mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT MOD_CODE, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE;
        con = new GroupByCondition("mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT MOD_CODE, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY MOD_CODE;
        con = new GroupByCondition("mod_code");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("mod_code"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
     
        return result;
    }

    public static ArrayList<OperatorNode> OneGroupingAttribute_time_id() throws Exception {
        ArrayList<OperatorNode> result = new ArrayList<OperatorNode>();
        TableNode c = GetStarSchemaTables().get("charter_fact");
        
        // SELECT TIME_ID, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID;
        GroupByCondition con = new GroupByCondition("time_id");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        GroupByOperatorNode g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        ProjectionOperatorNode p = new ProjectionOperatorNode(new ProjectionCondition("time_id"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT TIME_ID, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID;
        con = new GroupByCondition("time_id");
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT TIME_ID, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID;
        con = new GroupByCondition("time_id");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT TIME_ID, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID;
        con = new GroupByCondition("time_id");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT TIME_ID, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID;
        con = new GroupByCondition("time_id");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT TIME_ID, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID;
        con = new GroupByCondition("time_id");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT TIME_ID, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY TIME_ID;
        con = new GroupByCondition("time_id");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("time_id"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
     
        return result;
    }
    
    public static ArrayList<OperatorNode> OneGroupingAttribute_emp_num() throws Exception {
        ArrayList<OperatorNode> result = new ArrayList<OperatorNode>();
        TableNode c = GetStarSchemaTables().get("charter_fact");
        
        // SELECT EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY EMP_NUM;
        GroupByCondition con = new GroupByCondition("emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        GroupByOperatorNode g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        ProjectionOperatorNode p = new ProjectionOperatorNode(new ProjectionCondition("emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT EMP_NUM, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY EMP_NUM;
        con = new GroupByCondition("emp_num");
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS
        // FROM CHARTER_FACT
        // GROUP BY EMP_NUM;
        con = new GroupByCondition("emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT EMP_NUM, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY EMP_NUM;
        con = new GroupByCondition("emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(TOT_FUEL) AS TOT_FUEL
        // FROM CHARTER_FACT
        // GROUP BY EMP_NUM;
        con = new GroupByCondition("emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT EMP_NUM, SUM(TOT_FUEL) AS TOT_FUEL, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY EMP_NUM;
        con = new GroupByCondition("emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_fuel", AggregateFunctionEnum.SUM, "tot_fuel"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
        
        // SELECT EMP_NUM, SUM(TOT_CHAR_HOURS) AS TOT_CHAR_HOURS, SUM(REVENUE) AS REVENUE
        // FROM CHARTER_FACT
        // GROUP BY EMP_NUM;
        con = new GroupByCondition("emp_num");
        con.aggregateFunctions.add(new AggregateFunction("tot_char_hours", AggregateFunctionEnum.SUM, "tot_char_hours"));
        con.aggregateFunctions.add(new AggregateFunction("revenue", AggregateFunctionEnum.SUM, "revenue"));

        g = new GroupByOperatorNode(con);
        g.AddChildren(c);

        p = new ProjectionOperatorNode(new ProjectionCondition("emp_num"));
        ((ProjectionCondition)p.condition).aggregateFunctions = con.aggregateFunctions;
        p.AddChildren(g);

        result.add(p);
        //
     
        return result;
    }
    
    public static HashMap<String, TableNode> GetStarSchemaTables() {
        HashMap<String, TableNode> tables = new HashMap<String, TableNode>();

        TableNode charter_fact = new TableNode("charter_fact", TableTypeEnum.FACT, "c", "time_id", "mod_code", "emp_num", "tot_char_hours", "tot_fuel", "revenue");
        TableNode charter_pilotdim = new TableNode("charter_pilotdim", TableTypeEnum.DIMENSION, "p", "emp_num", "pil_license", "pil_ratings", "pil_med_type", "pil_med_date", "pil_pt135_date");
        TableNode charter_modeldim = new TableNode("charter_modeldim", TableTypeEnum.DIMENSION, "m", "mod_code", "mod_manufacturer", "mod_name", "mod_seats", "mod_chg_mile", "mod_cruise", "mod_fuel");
        TableNode charter_timedim = new TableNode("charter_timedim", TableTypeEnum.DIMENSION, "t", "time_id", "time_month", "time_year");

        tables.put(charter_fact.tableName, charter_fact);
        tables.put(charter_pilotdim.tableName, charter_pilotdim);
        tables.put(charter_modeldim.tableName, charter_modeldim);
        tables.put(charter_timedim.tableName, charter_timedim);

        return tables;
    }
*/
}
