package querytree.structure.conditions;

import java.util.ArrayList;
import java.util.Arrays;

public class SelectionCondition implements Condition{
    // A selection condition can contain 1...m statements of the
    // form (A=B,A>B,etc...).
    // It assumed that we consider only CONJUNCTIVE selection conditions
    // as the queries are OLAP queries.
    public ArrayList<Statement> statements;    
    
    public SelectionCondition(Statement statement){
        this.statements = new ArrayList<>();
        this.statements.add(statement);
    }
        
    public SelectionCondition(Statement... statements){
        this.statements = new ArrayList<Statement>(Arrays.asList(statements)); 
    }
    
    @Override
    public String toString(){
        return statements.toString();
    }
}