package querytree.structure.conditions;

import java.util.Objects;
import querytree.enums.RelationalOperatorEnum;
import querytree.utilities.LevenshteinDistance;

public class Statement{
    public String leftExpression;
    public String rightExpression;
    public RelationalOperatorEnum relationalOperator;
    
    public Statement(String leftExpression, String rightExpression, RelationalOperatorEnum relationalOperator){
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
        this.relationalOperator = relationalOperator;                
    }
    
    @Override
    public String toString(){
        return leftExpression + relationalOperator + rightExpression;
    }
    
    @Override
    public boolean equals(Object o){
        if(o == null || o.getClass() != Statement.class){
            return false;
        }
        else{
            Statement comparing = (Statement)o;
            return (LevenshteinDistance.levenshteinDistance(leftExpression, comparing.leftExpression) == 0) && 
                   (LevenshteinDistance.levenshteinDistance(rightExpression, comparing.rightExpression) == 0) && 
                   (LevenshteinDistance.levenshteinDistance(relationalOperator.toString(), comparing.relationalOperator.toString()) == 0);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.leftExpression);
        hash = 67 * hash + Objects.hashCode(this.rightExpression);
        hash = 67 * hash + Objects.hashCode(this.relationalOperator);
        return hash;
    }
}
