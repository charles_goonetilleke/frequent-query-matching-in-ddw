package querytree.structure.conditions;

import java.util.ArrayList;

public class ProjectionCondition implements Condition{
    public ArrayList<String> columnNames;
    public ArrayList<AggregateFunction> aggregateFunctions;
    
    public ProjectionCondition(String... columnNames){
        this.columnNames = new ArrayList<String>();
        for (String column : columnNames){
            this.columnNames.add(column.toUpperCase());
        }
        this.aggregateFunctions = new ArrayList<AggregateFunction>();
    }
   
    @Override
    public String toString() {
        return columnNames.toString() + aggregateFunctions.toString();
    }
}