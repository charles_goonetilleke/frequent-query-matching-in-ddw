package querytree.structure.conditions;

import java.util.ArrayList;
import java.util.Arrays;

public class GroupByCondition implements Condition {
    public ArrayList<String> groupingAttributes;
    public ArrayList<AggregateFunction> aggregateFunctions;

    public GroupByCondition(String... groupingAttributes) {
        this.groupingAttributes = new ArrayList<String>(Arrays.asList(groupingAttributes));
        this.aggregateFunctions = new ArrayList<AggregateFunction>();
    }
    
    @Override
    public String toString() {
        return groupingAttributes.toString() + "G" + aggregateFunctions.toString();
    }
}
