package querytree.structure.conditions;

import querytree.enums.AggregateFunctionEnum;
import java.util.Objects;
import querytree.enums.RelationalOperatorEnum;
import querytree.utilities.LevenshteinDistance;

public class AggregateFunction {

    public AggregateFunctionEnum functionType;
    public String parameter;
    public RelationalOperatorEnum relationalOperator;
    public String value;
    public String alias;

    public AggregateFunction(String alias, AggregateFunctionEnum functionType, String parameter) {
        this.alias = alias.toUpperCase();
        this.functionType = functionType;
        this.parameter = parameter.toUpperCase();
    }

    public AggregateFunction(String alias, AggregateFunctionEnum functionType, String parameter, RelationalOperatorEnum relationalOperator, String value) {
        this.alias = alias.toUpperCase();
        this.functionType = functionType;
        this.parameter = parameter.toUpperCase();
        this.relationalOperator = relationalOperator;
        this.value = value.toUpperCase();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || o.getClass() != AggregateFunction.class) {
            return false;
        } 
        else {
            AggregateFunction f = (AggregateFunction)o;
            return LevenshteinDistance.levenshteinDistance(f.functionType.toString(),this.functionType.toString()) == 0 && LevenshteinDistance.levenshteinDistance(f.parameter,this.parameter) == 0;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.functionType);
        hash = 19 * hash + Objects.hashCode(this.parameter);
        hash = 19 * hash + Objects.hashCode(this.relationalOperator);
        hash = 19 * hash + Objects.hashCode(this.value);
        hash = 19 * hash + Objects.hashCode(this.alias);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(this.functionType.name()).append("(").append(this.parameter).append(")");

        if (this.relationalOperator != null) {
            s.append(this.relationalOperator).append(this.value);
        } else {
            s.append(" -> ").append(this.alias);
        }

        return s.toString();
    }
}
