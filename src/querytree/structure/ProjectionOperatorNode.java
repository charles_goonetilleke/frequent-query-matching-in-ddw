package querytree.structure;

import querytree.structure.conditions.ProjectionCondition;
import querytree.enums.OperatorNodeTypeEnum;

public class ProjectionOperatorNode extends OperatorNode {

    public ProjectionOperatorNode(ProjectionCondition projectionCondition, BaseNode child) throws Exception {
        super(OperatorNodeTypeEnum.PROJECTION, projectionCondition, child);
    }
}
