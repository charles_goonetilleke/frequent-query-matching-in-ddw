package querytree.structure;

import java.util.ArrayList;
import querytree.enums.TableTypeEnum;

public class TableNode extends BaseNode{
    public String tableName;
    public ArrayList<String> columnNames;
    public String prefix;
    public TableTypeEnum tableType;
    
    public TableNode(String tableName, TableTypeEnum tableType, String prefix, String... columnNames){
        this.tableName = tableName;
        this.columnNames = new ArrayList<String>();
        this.prefix = prefix;
        this.tableType = tableType;
        
        for (String columnName : columnNames){
            this.columnNames.add(prefix.toUpperCase() + "." + columnName.toUpperCase());
        }
    }
    
    @Override
    public String toString(){
        return this.prefix;
    }
}
