package querytree.structure;

import querytree.structure.conditions.GroupByCondition;
import querytree.enums.OperatorNodeTypeEnum;

public class GroupByOperatorNode extends OperatorNode {
    
    public GroupByOperatorNode(GroupByCondition aggregateCondition, BaseNode child) throws Exception{
        super(OperatorNodeTypeEnum.GROUPBY, aggregateCondition, child);
    }
}