package querytree.structure;

import querytree.structure.conditions.SelectionCondition;
import querytree.enums.OperatorNodeTypeEnum;

public class SelectionOperatorNode extends OperatorNode {
    
    public SelectionOperatorNode(SelectionCondition selectionCondition, BaseNode child) throws Exception{
        super(OperatorNodeTypeEnum.SELECTION, selectionCondition, child);
    }
}