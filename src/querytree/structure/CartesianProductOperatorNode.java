package querytree.structure;

import java.util.ArrayList;
import querytree.enums.OperatorNodeTypeEnum;
import querytree.enums.TableTypeEnum;

public class CartesianProductOperatorNode extends OperatorNode {

    public CartesianProductOperatorNode(BaseNode leftSide, BaseNode rightSide) throws Exception {
        super(OperatorNodeTypeEnum.CARTESIAN_JOIN, null, leftSide, rightSide);
    }

    @Override
    public ArrayList<String> GetTableNames() throws Exception {
        ArrayList<String> tableNames = new ArrayList<String>();
        if (this.children.get(0) instanceof TableNode) {
            tableNames.add(((TableNode) this.children.get(0)).tableName);
        } else {
            tableNames.addAll(((OperatorNode) this.children.get(0)).GetTableNames());
        }

        if (this.children.get(1) instanceof TableNode) {
            tableNames.add(((TableNode) this.children.get(1)).tableName);
        } else {
            tableNames.addAll(((OperatorNode) this.children.get(1)).GetTableNames());
        }

        return tableNames;
    }
}
