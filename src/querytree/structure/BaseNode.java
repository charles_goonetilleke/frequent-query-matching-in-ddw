package querytree.structure;

import java.util.ArrayList;

public abstract class BaseNode {
    // A table cannot be a parent,
    // hence the type is OperatorNode and not type of BaseNode
    // A node can have 0..1 parents
    public OperatorNode parent = null;
    
    // A node can have 0..2 children
    public ArrayList<BaseNode> children = null;
    
    @Override
    public abstract String toString();
}