package querytree.structure;

import java.util.ArrayList;
import querytree.structure.conditions.Condition;
import querytree.enums.OperatorNodeTypeEnum;

public abstract class OperatorNode extends BaseNode {

    public OperatorNodeTypeEnum operatorType;
    public Condition condition;

    public OperatorNode(OperatorNodeTypeEnum operatorType, Condition condition, BaseNode... children) throws Exception {
        this.operatorType = operatorType;
        this.condition = condition;        
        this.AddChildren(children);
    }

    private void AddChildren(BaseNode... children) throws Exception {
        boolean canAddChildren = CanAddChildren(this, children);
        if (canAddChildren) {
            for (BaseNode child : children) {
                child.parent = this;
            }

            this.children = new ArrayList<BaseNode>();

            if (this.operatorType == OperatorNodeTypeEnum.JOIN || this.operatorType == OperatorNodeTypeEnum.CARTESIAN_JOIN) {
                this.children.add(children[0]);
                this.children.add(children[1]);
            } else {
                this.children.add(children[0]);
                this.children.add(null);
            }
        } else {
            throw new Exception(this.operatorType + ": Invalid number of children for given operator type");
        }
    }
    
    public static boolean CanAddChildren(OperatorNode operatorNode, BaseNode... children) {
        if (operatorNode.children != null) {
            return false;
        }

        if (operatorNode.operatorType == OperatorNodeTypeEnum.JOIN || operatorNode.operatorType == OperatorNodeTypeEnum.CARTESIAN_JOIN) {
            return children.length == 2;
        } else {
            return children.length == 1;
        }
    }

    public ArrayList<String> GetTableNames() throws Exception {
        ArrayList<String> tableNames = new ArrayList<String>();
        if (this.children.get(0) instanceof TableNode) {
            tableNames.add(((TableNode) this.children.get(0)).tableName);
        } else {
            tableNames.addAll(((OperatorNode) this.children.get(0)).GetTableNames());
        }

        return tableNames;
    }   

    @Override
    public String toString() {
        return Character.toString(operatorType.toString().charAt(0));
    }
}
