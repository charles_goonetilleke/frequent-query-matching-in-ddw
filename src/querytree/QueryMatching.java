package querytree;

import querytree.structure.conditions.AggregateFunction;
import querytree.structure.conditions.Statement;
import querytree.structure.conditions.ProjectionCondition;
import querytree.structure.conditions.SelectionCondition;
import querytree.structure.conditions.GroupByCondition;
import querytree.structure.SelectionOperatorNode;
import querytree.structure.GroupByOperatorNode;
import querytree.structure.TableNode;
import querytree.structure.CartesianProductOperatorNode;
import querytree.structure.BaseNode;

public class QueryMatching {
    public MaterializedViewQueryTree treeFromBucket;
    public InputQueryTree inputTree;

    public QueryMatching(MaterializedViewQueryTree treeFromBucket, InputQueryTree inputTree) {
        this.treeFromBucket = treeFromBucket;
        this.inputTree = inputTree;
    }

    // Table matching is perfomed whilst searching the B+tree
    // The rest of the matching is calculated here
    public double Calculate() throws Exception {
        double score = 0;
        
        double selection = MatchSelectionCondition();
        if(selection < 1){
            return score;
        }
        score += selection;
        
        double groupbyAttributes = MatchGroupByCondition();
        if(groupbyAttributes < 1){
            return score;
        }
        score += groupbyAttributes;
        
        double groupbyFunctions = MatchAggregateFunction();
        if(groupbyFunctions < 1){
            return score;
        }
        score += groupbyFunctions;
        
        double projection = MatchProjectionCondition();
        if(projection < 1){
            return score;
        }
        score += projection;

        return score;
    }

    private double MatchSelectionCondition() throws Exception {
        SelectionCondition treeFromBucket_selectionCondition = GetConditionOfSelection(treeFromBucket.root);
        SelectionCondition inputTree_selectionCondition = GetConditionOfSelection(inputTree.root);

        // If both don't have a selection condition
        if (inputTree_selectionCondition == null && treeFromBucket_selectionCondition == null) {
            return 1;
        }

        // If tree from bucket doesn't have a selection bu the input qt does
        if (treeFromBucket_selectionCondition == null && inputTree_selectionCondition != null) {
            return 1;
        }
        
        // If tree from bucket has selection condition but the input qt doesn't
        if(treeFromBucket_selectionCondition != null && inputTree_selectionCondition == null) {
            return 0;
        }

        // If both have selection conditions, then compare them
        int numberOfMatches = 0;
        for (Statement s : treeFromBucket_selectionCondition.statements) {
            if (inputTree_selectionCondition.statements.contains(s)) {
                ++numberOfMatches;
            }
        }

        return numberOfMatches / treeFromBucket_selectionCondition.statements.size();
    }

    private SelectionCondition GetConditionOfSelection(BaseNode root) throws Exception {
        // For query trees with a single table or have
        // no selection operator node
        if (root instanceof CartesianProductOperatorNode || root instanceof TableNode) {
            return null;
        }

        if (root instanceof SelectionOperatorNode) {
            return (SelectionCondition) (((SelectionOperatorNode) root).condition);
        }

        return GetConditionOfSelection(root.children.get(0));
    }

    private double MatchGroupByCondition() throws Exception {
        GroupByCondition treeFromBucket_groupByCondition = GetConditionOfGroupBy(treeFromBucket.root);
        GroupByCondition inputTree_groupByCondition = GetConditionOfGroupBy(inputTree.root);

        // If both don't have a group-by condition
        if (inputTree_groupByCondition == null && treeFromBucket_groupByCondition == null) {
            return 1;
        }
        
        if (treeFromBucket_groupByCondition == null || inputTree_groupByCondition == null) {
            return 0;
        }

        int numberOfMatches = 0;
        for (String s : inputTree_groupByCondition.groupingAttributes) {
            if (treeFromBucket_groupByCondition.groupingAttributes.contains(s)) {
                ++numberOfMatches;
            }
        }

        return numberOfMatches / inputTree_groupByCondition.groupingAttributes.size();
    }

    private double MatchAggregateFunction() throws Exception {
        GroupByCondition treeFromBucket_groupByCondition = GetConditionOfGroupBy(treeFromBucket.root);
        GroupByCondition inputTree_groupByCondition = GetConditionOfGroupBy(inputTree.root);
        int numberOfMatches = 0;

         // If both don't have a group-by condition
        if (inputTree_groupByCondition == null && treeFromBucket_groupByCondition == null) {
            return 1;
        }
        
        if (treeFromBucket_groupByCondition == null || inputTree_groupByCondition == null) {
            return numberOfMatches;
        }

        for (AggregateFunction f : inputTree_groupByCondition.aggregateFunctions) {
            if (treeFromBucket_groupByCondition.aggregateFunctions.contains(f)) {
                ++numberOfMatches;
            }
        }

        return numberOfMatches / inputTree_groupByCondition.aggregateFunctions.size();
    }

    private GroupByCondition GetConditionOfGroupBy(BaseNode root) throws Exception {
        // For query trees with a single table or have
        // no groupby operator node
        if (root instanceof SelectionOperatorNode || root instanceof CartesianProductOperatorNode || root instanceof TableNode) {
            return null;
        }

        if (root instanceof GroupByOperatorNode) {
            return (GroupByCondition) (((GroupByOperatorNode) root).condition);
        }

        return GetConditionOfGroupBy(root.children.get(0));
    }

    private double MatchProjectionCondition() {
        ProjectionCondition treeFromBucket_projectionCondition = (ProjectionCondition) (treeFromBucket.root.condition);
        ProjectionCondition inputTree_projectionCondition = (ProjectionCondition) (inputTree.root.condition);
        int numberOfMatches = 0;

        if (treeFromBucket_projectionCondition == null || inputTree_projectionCondition == null) {
            return numberOfMatches;
        }

        for (String s : inputTree_projectionCondition.columnNames) {
            if (treeFromBucket_projectionCondition.columnNames.contains(s)) {
                ++numberOfMatches;
            }
        }

        return numberOfMatches / inputTree_projectionCondition.columnNames.size();
    }   
}