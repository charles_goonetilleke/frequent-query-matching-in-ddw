package querytree;

import querytree.structure.BaseNode;
import querytree.structure.GroupByOperatorNode;
import querytree.structure.OperatorNode;
import querytree.structure.ProjectionOperatorNode;
import querytree.structure.SelectionOperatorNode;

public class InputQueryTree {
    public String sql;
    public OperatorNode root;

    public InputQueryTree(OperatorNode root) {
        this.root = root;
    }
    
    public InputQueryTree(String sql, OperatorNode root) {
        this.sql = sql;
        this.root = root;
    }
    
    public InputQueryTree(String sql) {
        this.sql = sql;
    }
    
    public String getSql(){
        return sql;
    }
    
    public void setSql(String sql){
        this.sql = sql;
    }

    @Override
    public String toString() {
        return this.root.toString();
    }
    
    public String displayData(){
        BaseNode node = root;
        String s = "";
        while(node instanceof SelectionOperatorNode || node instanceof GroupByOperatorNode || node instanceof ProjectionOperatorNode){
            s += ((OperatorNode)node).condition.toString();
            s += "\n";
            node = node.children.get(0);
        }
        
        return s;
    }
}
