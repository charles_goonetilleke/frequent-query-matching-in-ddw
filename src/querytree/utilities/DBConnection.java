package querytree.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class DBConnection {

    private static final String databaseURL = "jdbc:oracle:thin:@localhost:1521:orcl";
    private static final String username = "C##Charles";
    private static final String password = "P@ssw0rd";

    private static Connection connection = null;

    private static void Connect() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(databaseURL, username, password);
        }
    }

    public static ResultSet ExecuteQuery(String sql) throws SQLException {
        Connect();
        PreparedStatement pre = connection.prepareStatement(sql);
        return pre.executeQuery(sql);
    }

    public static int ExecuteUpdate(String sql) throws SQLException {
        Connect();
        PreparedStatement pre = connection.prepareStatement(sql);
        return pre.executeUpdate(sql);
    }

    public static void Disconnect() throws SQLException {
        if (connection != null) {
            connection.close();
            connection = null;
        }
    }
}
