package querytree.enums;

public enum OperatorNodeTypeEnum {
    SELECTION, PROJECTION, JOIN, GROUPBY, GROUPBY_CUBE, GROUPBY_ROLLUP, CARTESIAN_JOIN
}
