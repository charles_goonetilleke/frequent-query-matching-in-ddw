package querytree.enums;

public enum RelationalOperatorEnum {
    EQUAL_TO, NOT_EQUAL_TO, GREATER_THAN, LESS_THAN, GREATER_THAN_OR_EQUAL_TO, LESS_THAN_OR_EQUAL_TO, LIKE;

    @Override
    public String toString() {
        String symbol = null;
        switch (this) {
            case EQUAL_TO:
                symbol = " = ";
                break;
            case NOT_EQUAL_TO:
                symbol = " <> ";
                break;
            case GREATER_THAN:
                symbol = " > ";
                break;
            case GREATER_THAN_OR_EQUAL_TO:
                symbol = " >= ";
                break;
            case LESS_THAN:
                symbol = " < ";
                break;
            case LESS_THAN_OR_EQUAL_TO:
                symbol = " <= ";
                break;
            case LIKE:
                symbol = " LIKE ";
                break;
        }
        return symbol;
    }
}
