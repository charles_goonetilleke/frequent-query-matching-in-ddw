package querytree.enums;

public enum StatusEnum {
    ACTIVE, INACTIVE, SOFT_DELETED
}