package querytree.enums;

public enum AggregateFunctionEnum {
    AVG, SUM, MIN, MAX, COUNT
}
